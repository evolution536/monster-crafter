﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Monhun.Entities
{
    /// <summary>
    /// Represents an entity. The base class for anything that is drawable.
    /// </summary>
    public abstract class Entity : IDrawable, IFocusable
    {
        protected Texture2D texture;
        protected Vector2 position;
        protected Vector2 virtualPosition;
        string name;
        bool isColliding;

        public Entity(string name, Texture2D texture, Vector2 position)
        {
            this.name = name;
            this.texture = texture;
            this.position = position;
            this.virtualPosition = position;
        }

        public abstract bool IsMoveable { get; }

        public abstract void Update(GameTime gameTime);
        public abstract void Draw(SpriteBatch spriteBatch);

        /// <summary>
        /// Gets the position the entity currently is at.
        /// </summary>
        public Vector2 Position
        {
            get
            {
                return this.position;
            }
            set
            {
                this.position = value;
            }
        }

        /// <summary>
        /// Gets the name assigned to the entity.
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }
        }

        /// <summary>
        /// Gets the rectangle as bounding box for this entity.
        /// </summary>
        public Rectangle EntityRect
        {
            get
            {
                return new Rectangle((int)this.virtualPosition.X, (int)this.virtualPosition.Y, this.texture.Bounds.Width, this.texture.Bounds.Height);
            }
        }

        /// <summary>
        /// Gets or sets whether the entity is currently colliding.
        /// </summary>
        public bool IsColliding
        {
            get { return this.isColliding; }
            set { this.isColliding = true; }
        }

        /// <summary>
        /// Actually moves the entity. Virtual movement is used for collision checks.
        /// </summary>
        public void VirtualEntityMove()
        {
            this.position = new Vector2(this.virtualPosition.X, this.virtualPosition.Y);
        }

        /// <summary>
        /// Restores the previous non-colliding position as virtual position.
        /// </summary>
        public void RestoreVirtualPosition()
        {
            this.virtualPosition = new Vector2(this.position.X, this.position.Y);
        }
    }
}