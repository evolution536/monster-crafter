using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;

namespace Monhun
{
    /// <summary>
    /// Represents a key that identifies a quest.
    /// </summary>
    public struct QuestKey
    {
        readonly string questName;
        readonly Dictionary<string, int> monsters;
        readonly List<string> rewards;

        /// <summary>
        /// Gets the friendly name of the quest.
        /// </summary>
        public string QuestName
        {
            get { return questName; }
        } 

        /// <summary>
        /// Gets the monster list that contains the monsters required to hunt in order to finish the quest.
        /// </summary>
        public Dictionary<string, int> Monsters
        {
            get { return monsters; }
        }

        /// <summary>
        /// Gets the list of rewarded items that will be given after completing the quest.
        /// </summary>
        public List<string> Rewards
        {
            get { return rewards; }
        }

        public QuestKey(Dictionary<string, int> p1, List<string> p2, string p3)
        {
            monsters = p1;
            rewards = p2;
            questName = p3;
        }
    }

    /// <summary>
    /// Represents the menu that enables the user to select a quest to start.
    /// </summary>
    public class MonsterMenu : IDrawable, IUpdateable
    {
        GameMain game;
        Dictionary<Rectangle, QuestKey> quests;
        string navLocation;
        Texture2D backgroundTexture;
        string currentStartedQuest = null;

        bool clickable = false;
        int timeout = 20;

        public void resetTimeLimit()
        {
            clickable = false;
            timeout = 20;
        }

        public MonsterMenu(GameMain game)
        {
            this.quests = game.Quests;
            this.game = game;

            this.backgroundTexture = game.Content.Load<Texture2D>("Images/MonCraftMenuBackground");
        }

        public void Update(GameTime gametime)
        {
            this.navLocation = "MonsterMenu";

            if (clickable)
            {
                TouchCollection col = TouchPanel.GetState();
                for (int i = 0; i < col.Count; i++)
                {
                    TouchLocation tl = col[i];
                    if ((tl.State == TouchLocationState.Pressed) || (tl.State == TouchLocationState.Moved))
                    {
                        Vector2 newPos = new Vector2(tl.Position.X, tl.Position.Y);

                        foreach (KeyValuePair<Rectangle, QuestKey> val in this.quests)
                        {
                            if (val.Key.Contains((int)newPos.X, (int)newPos.Y))
                            {
                                if (string.IsNullOrEmpty(this.currentStartedQuest))
                                {
                                    resetTimeLimit();

                                    AIManager.GetInstance(this.game).ClearAI();
                                    this.game.MainPlayer.resetPlayer();

                                    foreach (KeyValuePair<string, int> mons in val.Value.Monsters)
                                    {
                                        AIManager.GetInstance(this.game).MonsterSpawn(mons.Value, mons.Key);
                                        this.navLocation = "MonsterHunt";
                                        break;
                                    }

                                    this.game.CurrentQuestRewards = val.Value.Rewards;

                                    // Set the selected quest as currently started quest.
                                    this.currentStartedQuest = val.Value.QuestName;
                                }
                                else if (val.Value.QuestName == this.currentStartedQuest)
                                {
                                    this.navLocation = "MonsterHunt";
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                if (--timeout <= 0)
                    clickable = true;
            }
        }

        public void Draw(SpriteBatch batch)
        {
            batch.Begin();

            batch.Draw(this.backgroundTexture, this.backgroundTexture.Bounds, Color.White);

            if (string.IsNullOrEmpty(this.currentStartedQuest))
            {
                // No quest is yet started, draw every button.
                foreach (KeyValuePair<Rectangle, QuestKey> val in this.quests)
                {
                    batch.Draw(DrawingHelper.CreateRectangleTexture(this.game.GraphicsDevice, 260, 50, 1, 0, 0, Color.OrangeRed, Color.Red), val.Key, Color.White);
                    batch.DrawString(this.game.Font, val.Value.QuestName, new Vector2(55, val.Key.Y + 15), Color.White);
                }
            }
            else
            {
                // A quest has already been started, only draw the button of the started quest.
                foreach (KeyValuePair<Rectangle, QuestKey> val in this.quests)
                {
                    if (val.Value.QuestName == this.currentStartedQuest)
                    {
                        batch.Draw(DrawingHelper.CreateRectangleTexture(this.game.GraphicsDevice, 260, 50, 1, 0, 0, Color.OrangeRed, Color.Red), val.Key, Color.White);
                        batch.DrawString(this.game.Font, val.Value.QuestName, new Vector2(55, val.Key.Y + 15), Color.White);
                    }
                }
            }

            batch.End();
        }

        /// <summary>
        /// Indicates whether the start button has been pressed yet.
        /// </summary>
        public string ShouldCloseMenu
        {
            get
            {
                return this.navLocation;
            }
        }

        /// <summary>
        /// Gets or sets the quest currently started.
        /// </summary>
        public string CurrentStartedQuest
        {
            get { return this.CurrentStartedQuest; }
            set { this.currentStartedQuest = value; }
        }
    }
}