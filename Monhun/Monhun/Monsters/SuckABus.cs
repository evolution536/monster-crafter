﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Monhun.Entities;
using System;

namespace Monhun.Monsters
{
    /// <summary>
    /// Represents the SuckABus monster entity.
    /// </summary>
    public sealed class SuckABus : Entity, IArtificialIntelligence, ILivingEntity
    {
        GameMain game;
        int stepCount;
        int aiStepsMade;
        FacingDirection nextAIdirection;
        float healthPoints;

        /// <summary>
        /// Initializes a new instance of the SuckABus monster entity using specified parameters.
        /// </summary>
        /// <param name="game">The game parameter to load content.</param>
        /// <param name="name">The name of the entity.</param>
        /// <param name="texture">The texture associated with the entity.</param>
        /// <param name="position">The position the entity will be drawn at.</param>
        public SuckABus(GameMain game, string name, Texture2D texture, Vector2 position) : base(name, texture, position)
        {
            this.game = game;
            this.stepCount = 100;
            this.healthPoints = 200;

            // Initialize AI steps to 20, otherwise it will never calculate the first AI.
            this.aiStepsMade = 20;
        }

        /// <summary>
        /// Indicates that the SuckABus monster is moveable. Needed for proper collision detection.
        /// </summary>
        public override bool IsMoveable
        {
            get { return true; }
        }

        /// <summary>
        /// Gets or sets the amount of health points this SuckABus has left.
        /// </summary>
        public float HealthPoints
        {
            get
            {
                return this.healthPoints;
            }
            set
            {
                this.healthPoints = value;
            }
        }

        /// <summary>
        /// Updates the SuckABus monster entity.
        /// </summary>
        /// <param name="gameTime">The gametime parameter from the main class.</param>
        public override void Update(GameTime gameTime)
        {
            // When the monster is dead, remove it from both the entity manager and the AI manager in order to spawn a new one.
            if (this.healthPoints <= 0)
            {
                EntityManager.GetInstance(null).RemoveEntity(base.Name);
                AIManager.GetInstance(null).RemoveAIEntity(this);
                return;
            }

            if (this.IsColliding)
            {
                // Collision detected, restore previous position.
                this.virtualPosition = this.position;
            }
            else
            {
                // No collision detected, commit virtual position.
                this.position = virtualPosition;
            }

            // Calculate the AI moves for this update method, but only if there is no collision.
            this.CalculateAI();

            // Increment AI steps, otherwise the Potatis will only walk in one direction all the time.
            ++this.aiStepsMade;

            // Move in the desired direction.
            switch (this.nextAIdirection)
            {
                case FacingDirection.Left:
                    //Loopt links
                    this.virtualPosition.X -= (float)gameTime.ElapsedGameTime.Milliseconds / 10.0f * 1.0f;
                    if (stepCount < 400)
                    {
                        this.texture = this.game.Content.Load<Texture2D>("Images/Monster/SuckABus/Left" + stepCount.ToString().Substring(0, 1));
                        stepCount += 10;
                    }
                    else
                    {
                        this.texture = this.game.Content.Load<Texture2D>("Images/Monster/SuckABus/Left2");
                        stepCount = 100;
                    }
                    break;
                case FacingDirection.Right:
                    //Loopt rechts
                    this.virtualPosition.X += (float)gameTime.ElapsedGameTime.Milliseconds / 10.0f * 1.0f;
                    if (stepCount < 400)
                    {
                        this.texture = this.game.Content.Load<Texture2D>("Images/Monster/SuckABus/Right" + stepCount.ToString().Substring(0, 1));
                        stepCount += 10;
                    }
                    else
                    {
                        this.texture = this.game.Content.Load<Texture2D>("Images/Monster/SuckABus/Right2");
                        stepCount = 100;
                    }
                    break;
                case FacingDirection.Up:
                    //Loopt naar boven
                    this.virtualPosition.Y -= (float)gameTime.ElapsedGameTime.Milliseconds / 10.0f * 1.0f;
                    if (stepCount < 400)
                    {
                        this.texture = this.game.Content.Load<Texture2D>("Images/Monster/SuckABus/Up" + stepCount.ToString().Substring(0, 1));
                        stepCount += 10;
                    }
                    else
                    {
                        this.texture = this.game.Content.Load<Texture2D>("Images/Monster/SuckABus/Up2");
                        stepCount = 100;
                    }
                    break;
                case FacingDirection.Down:
                    //Loopt naar beneden
                    this.virtualPosition.Y += (float)gameTime.ElapsedGameTime.Milliseconds / 10.0f * 1.0f;
                    if (stepCount < 400)
                    {
                        this.texture = this.game.Content.Load<Texture2D>("Images/Monster/SuckABus/Down" + stepCount.ToString().Substring(0, 1));
                        stepCount += 10;
                    }
                    else
                    {
                        this.texture = this.game.Content.Load<Texture2D>("Images/Monster/SuckABus/Down2");
                        stepCount = 100;
                    }
                    break;
            }
        }

        /// <summary>
        /// Draws the SuckABus monster entity.
        /// </summary>
        /// <param name="spriteBatch">the spritebatch parameter from the main class.</param>
        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(this.texture, this.position, Color.White);
        }

        /// <summary>
        /// Calculates the AI for the next move of this monster.
        /// </summary>
        public void CalculateAI()
        {
            if (this.IsColliding)
            {
                switch (this.nextAIdirection)
                {
                    case FacingDirection.Left:
                        this.nextAIdirection = FacingDirection.Right;
                        break;
                    case FacingDirection.Up:
                        this.nextAIdirection = FacingDirection.Down;
                        break;
                    case FacingDirection.Right:
                        this.nextAIdirection = FacingDirection.Left;
                        break;
                    case FacingDirection.Down:
                        this.nextAIdirection = FacingDirection.Up;
                        break;
                }

                this.IsColliding = false;
                this.aiStepsMade = 0;
                return;
            }

            if (this.aiStepsMade >= 20) // Change this number to alter the amount of steps the AI will make before changing direction.
            {
                // Get the player. If it is near, move towards it.
                Player player = EntityManager.GetInstance(null).GetPlayer();
                if (Vector2.Distance(this.position, player.Position) < 300f)
                {
                    // Get the angle of the player and monster in degrees. (note, 0 ~ 180 and 0 ~ -180).
                    float angle = (float)(Math.Atan2(player.Position.Y - this.position.Y, player.Position.X - this.position.X) * (180 / Math.PI));
                    if ((angle >= 45) && (angle <= 135))
                    {
                        this.nextAIdirection = FacingDirection.Down;
                    }
                    else if (((angle >= 135) && (angle <= 180)) || ((angle <= -135) && (angle >= -180)))
                    {
                        this.nextAIdirection = FacingDirection.Left;
                    }
                    else if ((angle >= -135) && (angle <= -45))
                    {
                        this.nextAIdirection = FacingDirection.Up;
                    }
                    else
                    {
                        this.nextAIdirection = FacingDirection.Right;
                    }
                }
                else
                {
                    // The player is not in the line of sight of the potatis, move in a random direction.
                    Random randomDirection = new Random();
                    this.nextAIdirection = (FacingDirection)randomDirection.Next(0, 4);
                }

                // Resets the AI stepper, meaning the SuckABus monster will walk 10 steps in the same direction.
                this.aiStepsMade = 0;
            }
        }
    }
}