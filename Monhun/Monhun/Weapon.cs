﻿using System;
using Microsoft.Xna.Framework.Graphics;

namespace Monhun.Weapons
{
    /// <summary>
    /// Represents a weapon that can be yielded by the main player.
    /// </summary>
    public abstract class Weapon : IDrawable
    {
        public void Draw(SpriteBatch batch)
        {

        }
    }

    public sealed class GreatSword : Weapon
    {

    }

    public sealed class LongSword : Weapon
    {

    }

    public sealed class SwordAndShield : Weapon
    {

    }
}