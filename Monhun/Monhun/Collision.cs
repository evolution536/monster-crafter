﻿using Monhun.Entities;

namespace Monhun
{
    /// <summary>
    /// Represents a collision between two entities.
    /// </summary>
    public struct Collision
    {
        Entity moving;
        Entity colliding;

        public Collision(Entity moving, Entity colliding)
        {
            this.moving = moving;
            this.colliding = colliding;
        }

        /// <summary>
        /// Gets the moving entity in this collision case.
        /// </summary>
        public Entity Moving
        {
            get { return this.moving; }
        }

        /// <summary>
        /// Gets the colliding entity in this collision case.
        /// </summary>
        public Entity Colliding
        {
            get { return this.colliding; }
        }
    }
}