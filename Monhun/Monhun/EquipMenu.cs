using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Monhun.Weapons;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;


namespace Monhun
{
    /// <summary>
    /// Represents a key of an item that is equippable.
    /// </summary>
    public struct EquipKey
    {
        readonly string weaponName;
        readonly Weapon weapon;
        readonly Vector2 position;

        public EquipKey(Weapon p1, Vector2 p2, string p3)
        {
            weapon = p1;
            position = p2;
            weaponName = p3;
        }

        /// <summary>
        /// Gets the name associated to this weapon key.
        /// </summary>
        public string WeaponName
        {
            get { return this.weaponName; }
        }

        /// <summary>
        /// Gets the weapon associated to this weapon key.
        /// </summary>
        public Weapon Weapon
        {
            get { return this.weapon; }
        }

        /// <summary>
        /// Gets the position associated to this weapon key.
        /// </summary>
        public Vector2 Position
        {
            get { return this.position; }
        }
    }

    /// <summary>
    /// Represents the in-game menu to equip new items for the player and inventorise current equipment.
    /// </summary>
    public sealed class EquipMenu : IDrawable, IUpdateable
    {
        GameMain game;
        string navLocation;
        bool selectedAWeapon;
        EquipKey selectedWeapon;
        Dictionary<Texture2D, EquipKey> weaponList;
        int buttonTimeout = 10;
        Texture2D equipButton;
        Rectangle equipButtonRect;
        bool equipedWeapon = false;
        bool reloadWeapons = true;
        Texture2D backgroundTexture;

        public bool ReloadWeapons
        {
            get { return reloadWeapons; }
            set { reloadWeapons = value; }
        }
        
        /// <summary>
        /// Initializes a new instance of the EquipMenu class using specified parameter.
        /// </summary>
        /// <param name="game">The game parameter needed to access globally instantiated objects.</param>
        public EquipMenu(GameMain game)
        {
            this.game = game;
            this.selectedAWeapon = false;
            weaponList = new Dictionary<Texture2D, EquipKey>(); 
            this.equipButton = DrawingHelper.CreateRectangleTexture(this.game.GraphicsDevice, 400, 60, 1, 0, 0, Color.OrangeRed, Color.Red);
            this.equipButtonRect = new Rectangle(300, 400, this.equipButton.Bounds.Width, this.equipButton.Bounds.Height);

            this.backgroundTexture = game.Content.Load<Texture2D>("Images/MonCraftMenuBackground");
        }

        /// <summary>
        /// Updates the menu.
        /// </summary>
        /// <param name="gametime">The gametime parameter from the main class.</param>
        public void Update(GameTime gametime)
        {
            this.navLocation = "Equipment";

            //if (weaponList.Count != this.game.EquipWeapons.Count)
            if(reloadWeapons)
            {
                //retrieve weapons
                int spacing = 50;
                for (int i = 0; i < this.game.EquipWeapons.Count; i++)
                {
                    weaponList.Add(DrawingHelper.CreateRectangleTexture(this.game.GraphicsDevice, 200, 60, 1, 0, 0, Color.OrangeRed, Color.Red), new EquipKey(Weapon.GetInstance(this.game).getWeapon(this.game.EquipWeapons[i].Replace(" ", "")), new Vector2(50, spacing), this.game.EquipWeapons[i]));
                    spacing += 80;
                }
                reloadWeapons = false;
            }

            if (this.buttonTimeout <= 9)
            {
                if (--this.buttonTimeout <= 0)
                {
                    this.buttonTimeout = 10;
                }
            }

            TouchCollection col = TouchPanel.GetState();
            for (int i = 0; i < col.Count; i++)
            {
                TouchLocation tl = col[i];
                if ((tl.State == TouchLocationState.Pressed) || (tl.State == TouchLocationState.Moved) && this.buttonTimeout >= 10)
                {
                    Vector2 newPos = new Vector2(tl.Position.X, tl.Position.Y);

                    foreach (KeyValuePair<Texture2D, EquipKey> drawing in this.weaponList)
                    {
                        if (new Rectangle((int)drawing.Value.Position.X, (int)drawing.Value.Position.Y, 200, 60).Contains((int)newPos.X, (int)newPos.Y))
                        {
                            selectedWeapon = drawing.Value;
                            selectedAWeapon = true;
                        }
                    }

                    if (this.equipButtonRect.Contains((int)newPos.X, (int)newPos.Y))
                    {
                        this.game.MainPlayer.WeaponName = selectedWeapon.WeaponName;
                        this.game.MainPlayer.Weapon = selectedWeapon.Weapon;
                    }


                    if (this.game.MainPlayer.WeaponName == selectedWeapon.WeaponName)
                        this.equipedWeapon = true;
                    else
                        this.equipedWeapon = false;

                    --this.buttonTimeout;
                }
            }
        }

        /// <summary>
        /// Draws the menu.
        /// </summary>
        /// <param name="spriteBatch">The spritebatch parameter from the main draw method.</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();

            spriteBatch.Draw(this.backgroundTexture, this.backgroundTexture.Bounds, Color.White);

            foreach (KeyValuePair<Texture2D, EquipKey> drawing in this.weaponList)
            {
                spriteBatch.Draw(drawing.Key, drawing.Value.Position, Color.White);
                spriteBatch.DrawString(this.game.Font, drawing.Value.WeaponName, new Vector2(55, (float)(drawing.Value.Position.Y + 20)), Color.White);
            }

            if (selectedAWeapon)
            {
                spriteBatch.Draw(DrawingHelper.CreateRectangleTexture(this.game.GraphicsDevice, 325, 60, 1, 0, 0, Color.OrangeRed, Color.Red), new Vector2(300, 50), Color.White);
                spriteBatch.DrawString(this.game.Font, selectedWeapon.WeaponName, new Vector2(305, 70), Color.White, 0f, new Vector2(0, 0), 1.2f, SpriteEffects.None, 0f);
                spriteBatch.DrawString(this.game.Font, "Power: " + selectedWeapon.Weapon.Damage, new Vector2(305, 120), Color.OrangeRed);
                spriteBatch.Draw(this.game.Content.Load<Texture2D>("Images/Weapons/" + selectedWeapon.Weapon.GetType().Name + "/" + selectedWeapon.Weapon.Name + "/Logo"), new Vector2(300, 160), Color.White);

                //Craft button
                spriteBatch.Draw(this.equipButton, this.equipButtonRect, Color.White);
                spriteBatch.DrawString(this.game.Font, "Equip", new Vector2(480, 420), Color.White, 0f, new Vector2(0, 0), 1.2f, SpriteEffects.None, 0f);

                // Not all materials are available, display error message.
                if (this.equipedWeapon)
                {
                    spriteBatch.DrawString(this.game.Font, "Equiped!", new Vector2(50, 420), Color.White);
                }
            }
            spriteBatch.End();
        }

        /// <summary>
        /// Indicates whether the start button has been pressed yet.
        /// </summary>
        public string ShouldCloseMenu
        {
            get
            {
                return this.navLocation;
            }
        }
    }
}