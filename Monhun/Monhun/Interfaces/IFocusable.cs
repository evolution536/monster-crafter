﻿using Microsoft.Xna.Framework;

namespace Monhun
{
    /// <summary>
    /// Represents an object that can be used as focus point for the camera.
    /// </summary>
    public interface IFocusable
    {
        /// <summary>
        /// Gets or sets the position of the focusable object.
        /// </summary>
        Vector2 Position { get; }
    }
}