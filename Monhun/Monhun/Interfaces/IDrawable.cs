using Microsoft.Xna.Framework.Graphics;

namespace Monhun
{
    /// <summary>
    /// Defines an interface that describes that classes are drawable.
    /// </summary>
    public interface IDrawable
    {
        /// <summary>
        /// Draws the content of the drawable class object using a specified spritebatch.
        /// </summary>
        /// <param name="batch">The spritebatch needed to draw the components.</param>
        void Draw(SpriteBatch batch);
    }
}
