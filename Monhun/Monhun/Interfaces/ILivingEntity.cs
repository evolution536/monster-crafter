﻿namespace Monhun
{
    /// <summary>
    /// Indicates that an entity is living, meaning it has a HP property.
    /// </summary>
    public interface ILivingEntity
    {
        /// <summary>
        /// Gets or sets the health points this living entity has left.
        /// </summary>
        float HealthPoints { get; set; }
    }
}