﻿using Microsoft.Xna.Framework;

namespace Monhun
{
    /// <summary>
    /// Defines a bot that can be managed by the AIManager.
    /// </summary>
    public interface IArtificialIntelligence
    {
        /// <summary>
        /// Calculates the AI for the next update method.
        /// </summary>
        void CalculateAI();
    }
}