﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Monhun
{
    /// <summary>
    /// Represents an input controller that can be used to handle custom key input.
    /// </summary>
    public sealed class InputControllerManager : IDrawable
    {
        /// <summary>
        /// The maximum number of buttons allowed to be controlled by the manager.
        /// </summary>
        public const int BUTTONCOUNT = 6;

        public InputButton[] inputButtons;
        int buttonIndex = 0;

        public InputControllerManager()
        {
            // AANPASSEN ALS MEERDERE BUTTONS WORDEN TOEGEVOEGD.
            inputButtons = new InputButton[BUTTONCOUNT];
        }

        /// <summary>
        /// Adds a button to the array of buttons to be controlled.
        /// </summary>
        /// <param name="button">The button to add to the button control list.</param>
        /// <exception cref="System.ArgumentOutOfRangeException">Thrown when the index used is greater than the maximum items accepted.</exception>
        public void AddButton(InputButton button)
        {
            if (button != null)
            {
                inputButtons[buttonIndex++] = button;
            }
        }

        /// <summary>
        /// Gets a button at the specified index in the button control list.
        /// </summary>
        /// <param name="i">The index to get the button at.</param>
        /// <returns>The button at the specified index. Returns null when the specified index does not contain an item.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">Thrown when the index specified is greater than the maximum number of items.</exception>
        public InputButton this[int i]
        {
            get { return this.inputButtons[i]; }
        }

        public void Draw(SpriteBatch sp)
        {
            int count = 0;
            while (count < this.inputButtons.Length)
            {
                this.inputButtons[count++].Draw(sp);
            }
        }
    }

    /// <summary>
    /// Represents a button that can be drawn on-screen and used to handle user input.
    /// </summary>
    public class InputButton : IDrawable
    {
        Texture2D button;
        Rectangle position;

        public InputButton(Texture2D button, Rectangle position)
        {
            this.button = button;
            this.position = position;
        }

        public void Draw(SpriteBatch sp)
        {
            sp.Draw(button, position, Color.White);
        }

        /// <summary>
        /// Gets the bounds of the button as a Rectangle.
        /// </summary>
        public Rectangle Bounds
        {
            get
            {
                return this.position;
            }
        }
    }
}