﻿using System;

namespace Monhun
{
    /// <summary>
    /// This exception is thrown when the EntityManager detects a duplicate Entity about to be added.
    /// </summary>
    public sealed class EntityExistsException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the EntityExistsException.
        /// </summary>
        public EntityExistsException() : base("The entity already exists in the list!") { }
    }
}