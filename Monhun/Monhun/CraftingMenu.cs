﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Monhun.Weapons;
using Microsoft.Xna.Framework.Input.Touch;

namespace Monhun
{
    /// <summary>
    /// Represents a craftable item. It is used as a key in the algorithm.
    /// </summary>
    public struct CraftKey
    {
        readonly Craftable weapon;
        readonly Vector2 position;

        public CraftKey(Craftable p1, Vector2 p2)
        {
            weapon = p1;
            position = p2;
        }

        /// <summary>
        /// Gets the craftable instance associated to this craft key.
        /// </summary>
        public Craftable Weapon
        {
            get { return this.weapon; }
        }

        /// <summary>
        /// Gets the position associated to this craft key.
        /// </summary>
        public Vector2 Position
        {
            get { return this.position; }
        }
    }

    /// <summary>
    /// Represents the crafting menu in game.
    /// </summary>
    public sealed class CraftingMenu : IDrawable, IUpdateable
    {
        GameMain game;
        List<Craftable> craftables;
        Dictionary<Texture2D, CraftKey> craftableTextures = new Dictionary<Texture2D, CraftKey>();
        bool selectedAWeapon;
        Craftable selectedWeapon;
        string navLocation;
        Texture2D craftButton;
        Rectangle craftButtonRect;
        bool craftError;
        bool craftSuccess;
        bool showInventory;
        Texture2D inventoryButton;
        Rectangle inventoryButtonRect;
        int showItems = 12;
        int currentPage = 1;
        int buttonTimeout = 10;
        Texture2D backgroundTexture;
        Texture2D selectedWeaponTexture;
        Texture2D inventoryTitleTexture;

        /// <summary>
        /// Initializes a new instance of the CraftingMenu class using specified parameter.
        /// </summary>
        /// <param name="game">The gamemain pointer needed to refer to.</param>
        public CraftingMenu(GameMain game)
        {
            this.game = game;
            this.selectedAWeapon = false;
            this.showInventory = false;

            this.craftables = this.game.Craftables;
            int spacing = 50;
            for (int i = 0; i < this.craftables.Count; i++)
            {
                craftableTextures.Add(DrawingHelper.CreateRectangleTexture(this.game.GraphicsDevice, 200, 60, 1, 0, 0, Color.OrangeRed, Color.Red), new CraftKey(this.craftables[i], new Vector2(50, spacing)));
                spacing += 80;
            }

            this.craftButton = DrawingHelper.CreateRectangleTexture(this.game.GraphicsDevice, 400, 60, 1, 0, 0, Color.OrangeRed, Color.Red);
            this.craftButtonRect = new Rectangle(300, 400, this.craftButton.Bounds.Width, this.craftButton.Bounds.Height);

            this.inventoryButton = DrawingHelper.CreateRectangleTexture(this.game.GraphicsDevice, 130, 60, 1, 0, 0, Color.OrangeRed, Color.Red);
            this.inventoryButtonRect = new Rectangle(635, 50, this.inventoryButton.Bounds.Width, this.inventoryButton.Bounds.Height);

            this.backgroundTexture = game.Content.Load<Texture2D>("Images/MonCraftMenuBackground");
            this.selectedWeaponTexture = DrawingHelper.CreateRectangleTexture(this.game.GraphicsDevice, 325, 60, 1, 0, 0, Color.OrangeRed, Color.Red);
            this.inventoryTitleTexture = DrawingHelper.CreateRectangleTexture(this.game.GraphicsDevice, 575, 60, 1, 0, 0, Color.OrangeRed, Color.Red);
        }

        /// <summary>
        /// Updates the crafting menu.
        /// </summary>
        /// <param name="gameTime">The gametime parameter from the main class.</param>
        public void Update(GameTime gameTime)
        {
            this.navLocation = "Crafting";

            // Timeout to prevent reclicking the inventory and craft button too fast.
            if (this.buttonTimeout <= 9)
            {
                if (--this.buttonTimeout <= 0)
                {
                    this.buttonTimeout = 10;
                }
            }

            TouchCollection col = TouchPanel.GetState();
            for (int i = 0; i < col.Count; i++)
            {
                TouchLocation tl = col[i];
                if ((tl.State == TouchLocationState.Pressed) || (tl.State == TouchLocationState.Moved) && this.buttonTimeout >= 10)
                {
                    Vector2 newPos = new Vector2(tl.Position.X, tl.Position.Y);

                    foreach (KeyValuePair<Texture2D, CraftKey> drawing in this.craftableTextures)
                    {
                        if (new Rectangle((int)drawing.Value.Position.X, (int)drawing.Value.Position.Y, 200, 60).Contains((int)newPos.X, (int)newPos.Y))
                        {
                            selectedWeapon = drawing.Value.Weapon;
                            selectedAWeapon = true;
                            this.craftError = false;
                        }
                    }

                    if (this.craftButtonRect.Contains((int)newPos.X, (int)newPos.Y))
                    {
                        // Craft button clicked.
                        this.craftError = false;
                        var it = selectedWeapon.ItemsNeeded.GetEnumerator();
                        for (var cur = it.Current; it.MoveNext(); )
                        {
                            // Check amount of each needed item available.
                            if (this.game.Inventory[it.Current.Key] < this.selectedWeapon.ItemsNeeded[it.Current.Key])
                            {
                                this.craftError = true;
                                break;
                            }
                        }

                        this.craftSuccess = false;
                        if (!this.craftError)
                        {
                            this.craftSuccess = true;

                            // Remove used items from the inventory.
                            var it2 = selectedWeapon.ItemsNeeded.GetEnumerator();
                            for (var cur = it2.Current; it2.MoveNext(); )
                            {
                                int currentItem = it2.Current.Value;
                                if (currentItem > 1)
                                {
                                    // Remove 'count' items from the inventory.
                                    int index = 0;
                                    while (index < currentItem)
                                    {
                                        this.game.Inventory.RemoveItem(it2.Current.Key);
                                        ++index;
                                    }
                                }
                                else
                                {
                                    // Remove only one item from the inventory.
                                    this.game.Inventory.RemoveItem(it2.Current.Key);
                                }
                            }

                            this.game.addWeapon(selectedWeapon.Name);
                        }
                    }

                    // Check whether the inventory button is clicked.
                    if (this.inventoryButtonRect.Contains((int)newPos.X, (int)newPos.Y))
                    {
                        this.showInventory = !this.showInventory;
                    }

                    if (this.showInventory)
                    {
                        //25, spriteBatch.GraphicsDevice.Viewport.Height - 95, 75, 75
                        if (new Rectangle(205, 480 - 95, 75, 75).Contains((int)newPos.X, (int)newPos.Y))
                        {
                            if (this.currentPage <= (this.game.Inventory.Items.Count / this.showItems))
                            this.currentPage++;
                        }
                        if (new Rectangle(55, 480 - 95, 75, 75).Contains((int)newPos.X, (int)newPos.Y))
                        {
                            if(this.currentPage >= 2)
                                this.currentPage--;
                        }
                    }
                    --this.buttonTimeout;
                }
            }
        }

        /// <summary>
        /// Draws the crafting menu.
        /// </summary>
        /// <param name="spriteBatch">The spritebatch parameter from the main class.</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();

            // Background
            spriteBatch.Draw(this.backgroundTexture, this.backgroundTexture.Bounds, Color.White);

            //Inventory button
            spriteBatch.Draw(this.inventoryButton, this.inventoryButtonRect, Color.White);
            string inventoryText = this.showInventory ? "Close" : "Inventory";
            spriteBatch.DrawString(this.game.Font, inventoryText, new Vector2(640, 70), Color.White, 0f, new Vector2(0, 0), 1.2f, SpriteEffects.None, 0f);

            if (!this.showInventory)
            {
                foreach (KeyValuePair<Texture2D, CraftKey> drawing in this.craftableTextures)
                {
                    spriteBatch.Draw(drawing.Key, drawing.Value.Position, Color.White);
                    spriteBatch.DrawString(this.game.Font, drawing.Value.Weapon.Name, new Vector2(55, (float)(drawing.Value.Position.Y + 20)), Color.White);
                }

                if (selectedAWeapon)
                {
                    spriteBatch.Draw(this.selectedWeaponTexture, new Vector2(300, 50), Color.White);
                    spriteBatch.DrawString(this.game.Font, selectedWeapon.Name, new Vector2(305, 70), Color.White, 0f, new Vector2(0, 0), 1.2f, SpriteEffects.None, 0f);
                    spriteBatch.DrawString(this.game.Font, "Power: " + selectedWeapon.Damage, new Vector2(305, 120), Color.OrangeRed);
                    spriteBatch.Draw(selectedWeapon.Texture, new Vector2(300, 160), Color.White);

                    //Start Y texture + height + spacing
                    int spacing = 160 + 65 + 10;
                    spriteBatch.DrawString(this.game.Font, "Needed items:", new Vector2(305, spacing), Color.Green, 0f, new Vector2(0, 0), 1.1f, SpriteEffects.None, 0f);
                    spacing += 30;
                    //items needed
                    foreach (KeyValuePair<string, int> drawing in selectedWeapon.ItemsNeeded)
                    {
                        spriteBatch.DrawString(this.game.Font, drawing.Value + "X - " + drawing.Key, new Vector2(305, spacing), Color.OrangeRed);
                        spacing += 20;
                    }

                    //Craft button
                    spriteBatch.Draw(this.craftButton, this.craftButtonRect, Color.White);
                    spriteBatch.DrawString(this.game.Font, "Craft", new Vector2(480, 420), Color.White, 0f, new Vector2(0, 0), 1.2f, SpriteEffects.None, 0f);

                    // Not all materials are available, display error message.
                    if (this.craftError)
                    {
                        spriteBatch.DrawString(this.game.Font, "Not enough materials!", new Vector2(50, 435), Color.White);
                    }
                    
                    // Not all materials are available, display error message.
                    if (this.craftSuccess)
                    {
                        spriteBatch.DrawString(this.game.Font, "Weapon made!", new Vector2(50, 435), Color.White);
                    }
                }
            }
            else
            {
                spriteBatch.Draw(this.inventoryTitleTexture, new Vector2(50, 50), Color.White);
                spriteBatch.DrawString(this.game.Font, "Inventory", new Vector2(55, 70), Color.White, 0f, new Vector2(0, 0), 1.2f, SpriteEffects.None, 0f);

                int inventorySpacing = 120;

                foreach (KeyValuePair<string, int> drawing in this.game.Inventory.Items.Skip((this.currentPage - 1) * this.showItems).Take(this.showItems))
                {
                    spriteBatch.DrawString(this.game.Font, drawing.Value + "X - " + drawing.Key, new Vector2(55, inventorySpacing), Color.White);
                    inventorySpacing += 20;
                }

                spriteBatch.Draw(this.game.Content.Load<Texture2D>("Images/Direction/Left"), new Rectangle(55, 480 - 95, 75, 75), Color.White);
                int pageCount = ((this.game.Inventory.Items.Count / this.showItems) > 0) ? (this.game.Inventory.Items.Count / this.showItems) : 1;
                spriteBatch.DrawString(this.game.Font, this.currentPage + "/" + pageCount, new Vector2(148, 480 - 70), Color.White, 0f, new Vector2(0, 0), 1.2f, SpriteEffects.None, 0f);
                spriteBatch.Draw(this.game.Content.Load<Texture2D>("Images/Direction/Right"), new Rectangle(205, 480 - 95, 75, 75), Color.White);
            }

            spriteBatch.End();
        }

        /// <summary>
        /// Indicates whether the start button has been pressed yet.
        /// </summary>
        public string ShouldCloseMenu
        {
            get
            {
                return this.navLocation;
            }
        }
    }
}