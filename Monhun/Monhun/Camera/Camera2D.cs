﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Monhun
{
    /// <summary>
    /// Represents the camera that is used to view the user the needed play field.
    /// </summary>
    public sealed class Camera2D : ICamera2D, IUpdateable
    {
        Vector2 _position;
        float _viewportHeight;
        float _viewportWidth;
        GraphicsDevice mDevice;

        /// <summary>
        /// Initializes a new instance of the Camera2D class using specified parameters.
        /// </summary>
        /// <param name="pDevice">A pointer to the graphics device managed by GameMain.</param>
        /// <param name="focusObject">The IFocusable object that will be used by the camera as center point of view.</param>
        public Camera2D(GraphicsDevice pDevice, IFocusable focusObject)
        {
            this.mDevice = pDevice;
            this.Focus = focusObject;
        }

        /// <summary>
        /// Gets the current position of the camera.
        /// </summary>
        public Vector2 Position
        {
            get { return _position; }
        }

        /// <summary>
        /// Gets or sets the rotation of the camera.
        /// </summary>
        public float Rotation { get; set; }

        /// <summary>
        /// Gets the origin of the camera.
        /// </summary>
        public Vector2 Origin { get; private set; }

        /// <summary>
        /// Gets or sets the scale of the camera. Set this to zoom in or out. Note that a negative zoom will flip the image.
        /// </summary>
        public float Scale { get; set; }

        /// <summary>
        /// Gets the center coordinate of the screen, viewed from the camera.
        /// </summary>
        public Vector2 ScreenCenter { get; private set; }

        /// <summary>
        /// Gets the transform the camera calculated in the Update method. Needed to draw relatively to the camera.
        /// </summary>
        public Matrix Transform { get; private set; }

        /// <summary>
        /// Gets or sets the object that is focused by the camera.
        /// </summary>
        public IFocusable Focus { get; set; }

        /// <summary>
        /// Gets of sets the movement speed the camera will use.
        /// </summary>
        public float MoveSpeed { get; set; }

        /// <summary>
        /// Called when the GameComponent needs to be initialized.
        /// </summary>
        public void Initialize()
        {
            this._viewportWidth = this.mDevice.Viewport.Width;
            this._viewportHeight = this.mDevice.Viewport.Height;

            this.ScreenCenter = new Vector2(this._viewportWidth / 2, this._viewportHeight / 2);
            this.Scale = 1;
            this.MoveSpeed = 1.25f;
        }

        /// <summary>
        /// Updates the camera, which includes calculating transform, origin and new position.
        /// </summary>
        public void Update(GameTime gameTime)
        {
            this.Origin = this.ScreenCenter / this.Scale;

            // Create the Transform used by any spritebatch process
            this.Transform = Matrix.Identity * Matrix.CreateTranslation(-Position.X, -Position.Y, 0) *
                        Matrix.CreateRotationZ(Rotation) * Matrix.CreateTranslation(Origin.X, Origin.Y, 0) *
                        Matrix.CreateScale(new Vector3(Scale, Scale, Scale));

            // Move the Camera to the position that it needs to go
            var delta = (float)gameTime.ElapsedGameTime.TotalSeconds;

            this._position.X += (this.Focus.Position.X - this.Position.X) * this.MoveSpeed * delta;
            this._position.Y += (this.Focus.Position.Y - this.Position.Y) * this.MoveSpeed * delta;
        }

        /// <summary>
        /// Determines whether the target is in view given the specified position.
        /// This can be used to increase performance by not drawing objects directly in the viewport.
        /// </summary>
        /// <param name="positionRect">The position also defining width and height.</param>
        /// <returns>
        ///     <c>true</c> if [is in view] [the specified position]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsInView(Rectangle positionRect)
        {
            // If the object is not within the horizontal bounds of the screen
            if ((positionRect.X + positionRect.Width) < (this.Position.X - this.Origin.X) || (positionRect.X) > (this.Position.X + this.Origin.X))
            {
                return false;
            }

            // If the object is not within the vertical bounds of the screen
            if ((positionRect.Y + positionRect.Height) < (this.Position.Y - this.Origin.Y) || (positionRect.Y) > (this.Position.Y + this.Origin.Y))
            {
                return false;
            }

            return true;
        }
    }
}