﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;

namespace Monhun
{
    /// <summary>
    /// Represents the main menu that will be displayed on game start.
    /// </summary>
    public sealed class MainMenu : IDrawable, IUpdateable
    {
        SpriteFont mainMenuSpritefont;
        Texture2D background;
        Texture2D continueButton;
        Rectangle continueButtonRectangle;
        Texture2D newGameButton;
        Rectangle newGameButtonRectangle;
        GameMain game;

        string shouldCloseMenu = "MainMenu";

        /// <summary>
        /// Initializes a new instance of the MainMenu class using specified parameters.
        /// </summary>
        /// <param name="game">The GameMain class needed to switch between menu and gameplay</param>
        public MainMenu(GameMain game)
        {
            this.game = game;
            this.mainMenuSpritefont = game.Content.Load<SpriteFont>("MainMenu");
            this.background = game.Content.Load<Texture2D>("Images/MonCraftMenuBackground");
            this.continueButton = DrawingHelper.CreateRectangleTexture(game.GraphicsDevice, 250, 50, 1, 1, 1, Color.OrangeRed, Color.Red);
            this.continueButtonRectangle = new Rectangle((game.Window.ClientBounds.Width / 2) + 50, 125, this.continueButton.Width, this.continueButton.Height);
            this.newGameButton = DrawingHelper.CreateRectangleTexture(game.GraphicsDevice, 250, 50, 1, 1, 1, Color.OrangeRed, Color.Red);
            this.newGameButtonRectangle = new Rectangle((game.Window.ClientBounds.Width / 2) + 50, 250, this.newGameButton.Width, this.newGameButton.Height);
        }

        /// <summary>
        /// Update sequence for the MainMenu class.
        /// </summary>
        /// <param name="gametime">The gametime parameter passed from the main update method.</param>
        public void Update(GameTime gametime)
        {
            this.shouldCloseMenu = "MainMenu";

            TouchCollection col = TouchPanel.GetState();
            for (int i = 0; i < col.Count; i++)
            {
                TouchLocation tl = col[i];
                if (tl.State == TouchLocationState.Pressed)
                {
                    Vector2 newPos = new Vector2(tl.Position.X, tl.Position.Y);

                    if (this.continueButtonRectangle.Contains((int)newPos.X, (int)newPos.Y))
                    {
                        this.game.loadSaveFile();
                        this.shouldCloseMenu = "GameMenu";
                        break;
                    }
                    else if (this.newGameButtonRectangle.Contains((int)newPos.X, (int)newPos.Y))
                    {
                        this.shouldCloseMenu = "GameMenu";
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Draws the main menu.
        /// </summary>
        /// <param name="batch">The spritebatch parameter passed from the main draw method.</param>
        public void Draw(SpriteBatch batch)
        {
            batch.Begin();
            batch.Draw(this.background, background.Bounds, Color.White);
            batch.DrawString(this.mainMenuSpritefont, "Monster Crafter", new Vector2(200f, 30f), Color.Red);
            batch.Draw(this.continueButton, continueButtonRectangle, Color.White);
            batch.DrawString(this.game.Font, "Continue", new Vector2((game.Window.ClientBounds.Width / 2) + 75, 135f), Color.Blue, 0f, new Vector2(0, 0), 1.5f, SpriteEffects.None, 0f);
            batch.Draw(this.newGameButton, this.newGameButtonRectangle, Color.White);
            batch.DrawString(this.game.Font, "New Game", new Vector2((game.Window.ClientBounds.Width / 2) + 75, 260f), Color.Blue, 0f, new Vector2(0, 0), 1.5f, SpriteEffects.None, 0f);

            batch.End();
        }

        /// <summary>
        /// Indicates whether the start button has been pressed yet.
        /// </summary>
        public string ShouldCloseMenu
        {
            get
            {
                return this.shouldCloseMenu;
            }
        }
    }
}