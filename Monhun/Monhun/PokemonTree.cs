﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Monhun.Entities
{
    /// <summary>
    /// Represents a pokemon tree that is drawn as a nice tree on the map.
    /// </summary>
    public sealed class PokemonTree : Entity
    {
        public PokemonTree(string name, Texture2D texture, Vector2 center) : base(name, texture, center)
        {

        }

        /// <summary>
        /// Indicates that a Pokemon tree is not moveable. Needed for proper collision detection.
        /// </summary>
        public override bool IsMoveable
        {
            get { return false; }
        }

        public override void Update(GameTime gameTime)
        {
            // a tree doesn't move
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(this.texture, this.position, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
        }
    }
}