using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Monhun.Entities;
using Monhun.Skills;
using Monhun.Weapons;
using System.Collections.Generic;
using System.Threading;
using Microsoft.Xna.Framework.Media;

namespace Monhun
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public sealed class GameMain : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Camera2D camera;
        EntityManager entityManager;
        AIManager aiManager;
        InputControllerManager inputManag;
        MainMenu mainMenu;
        CraftingMenu craftingMenu;
        EquipMenu equipmentMenu;
        MonsterMenu monsterMenu;
        SettingsMenu settingsMenu;
        GameMenu gameMenu;
        Entity mainplayer;
        string navLocation;
        SpriteFont spriteFont;
        IndicationBar healthBar;
        IndicationBar manaBar;
        Inventory inventory;
        List<string> equipWeapons;
        SaveFile saveFile;

        //Sound
        SoundEffect menuSound;
        SoundEffectInstance menuSoundInstance;
        SoundEffect monsterSound;
        SoundEffectInstance monsterSoundInstance;

        // Background Textures
        Texture2D backgroundTextureGrass;
        Texture2D backgroundTextureSnow;
        Texture2D backgroundTextureDesert;
        Texture2D backgroundTextureMetal;

        // Background texture positions
        float grassBGx;
        float grassBGy;
        float UpperBaseY;
        float RightBaseX;
        float MiddleBaseX;
        float DownBaseY;

        //Duration of showing the fail image
        int failtime = 75;
        bool shouldEndGame;

        List<string> currentQuestRewards;
        bool questClear = false;
        Dictionary<Rectangle, QuestKey> quests;
        List<Craftable> craftables;

        bool backgroundMusic;

        public List<Craftable> Craftables
        {
            get { return craftables; }
        }

        /// <summary>
        /// Initializes a new instance of the GameMain class.
        /// </summary>
        public GameMain()
        {
            this.graphics = new GraphicsDeviceManager(this);
            this.graphics.IsFullScreen = true;
            this.Content.RootDirectory = "Content";
            equipWeapons = new List<string>() { "NoobSlasher" };

            // Frame rate is 30 fps by default for Windows Phone.
            this.TargetElapsedTime = TimeSpan.FromTicks(333333);

            // Extend battery life under lock.
            this.InactiveSleepTime = TimeSpan.FromSeconds(1);

            // Set fullscreen window size to WVGA (WP7)
            this.graphics.PreferredBackBufferHeight = 480;
            this.graphics.PreferredBackBufferWidth = 800;

            // Set multisampling to enabled and force landscape view for the phone.
            this.graphics.SupportedOrientations = DisplayOrientation.LandscapeLeft;
            this.graphics.PreferMultiSampling = true;

            this.craftables = new List<Craftable>();
            this.quests = new Dictionary<Rectangle, QuestKey>();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content. Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            this.navLocation = "MainMenu";

            this.inputManag = new InputControllerManager();
            this.aiManager = AIManager.GetInstance(this);

            backgroundMusic = true;
            if (!MediaPlayer.GameHasControl)
            {
                Settings.settingVolume = 0;
                backgroundMusic = false;
            }

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            this.inventory = new Inventory();
            this.spriteFont = this.Content.Load<SpriteFont>("Arial");

            this.menuSound = Content.Load<SoundEffect>("Sounds/PencilMaze");
            this.menuSoundInstance = this.menuSound.CreateInstance();
            this.menuSoundInstance.IsLooped = true;

            this.monsterSound = Content.Load<SoundEffect>("Sounds/FinalAct");
            this.monsterSoundInstance = this.monsterSound.CreateInstance();
            this.monsterSoundInstance.IsLooped = true;

            this.craftables.Add(new Craftable("Potatis Poker", this.Content.Load<Texture2D>("Images/Weapons/LongSword/PotatisPoker/Logo"), 10, new Dictionary<string, int>() { { "Potatis Tail", 3 }, { "Potatis Eye", 1 }, { "Potatis Leg", 2 } }));
            this.craftables.Add(new Craftable("Bus Slasher", this.Content.Load<Texture2D>("Images/Weapons/LongSword/BusSlasher/Logo"), 15, new Dictionary<string, int>() { { "SuckABus Tail", 6 }, { "SuckABus Arm", 9 }, { "SuckABus Beams", 12 } }));
            this.craftables.Add(new Craftable("Random Stabber", this.Content.Load<Texture2D>("Images/Weapons/LongSword/RedGreenRandomStabber/Logo"), 20, new Dictionary<string, int>() { { "Green Red Tail", 8 }, { "Red Wings", 5 }, { "Dragon Breath", 7 }, { "Red Green Nails", 9 } }));
            this.craftables.Add(new Craftable("Invisible Poker", this.Content.Load<Texture2D>("Images/Weapons/LongSword/InvisiblePoker/Logo"), 25, new Dictionary<string, int>() { { "Invisible Scale", 12 }, { "Invisible Arm", 7 }, { "Invisible Sharpener", 9 }, { "Invisible Blade", 6 } }));
            this.craftables.Add(new Craftable("Bawb Ghais Toenail", this.Content.Load<Texture2D>("Images/Weapons/LongSword/BawbGhaisToenail/Logo"), 30, new Dictionary<string, int>() { { "Bawb Toenail", 15 }, { "Ghais Toenail", 15 }, { "Bawb Ghais Glue", 5 }, { "Bawb Feet", 3 }, { "Ghais Feet", 3 } }));

            // For every Rectangle + 70
            this.quests.Add(new Rectangle(50, 50, 250, 50), new QuestKey(new Dictionary<string, int>() { { "Potatis", 5 } }, new List<string>() { "Potatis Tail", "Potatis Eye", "Potatis Leg" }, "Hunt 5 Potatis"));
            this.quests.Add(new Rectangle(50, 120, 250, 50), new QuestKey(new Dictionary<string, int>() { { "SuckABus", 4 } }, new List<string>() { "SuckABus Tail", "SuckABus Arm", "SuckABus Beams" }, "Hunt 4 SuckABus"));
            this.quests.Add(new Rectangle(50, 190, 250, 50), new QuestKey(new Dictionary<string, int>() { { "RedWingsGreenDragon", 3 } }, new List<string>() { "Green Red Tail", "Red Wings", "Dragon Breath", "Dragon Breath", "Red Green Nails" }, "Hunt 3 Red Green Dragons"));
            this.quests.Add(new Rectangle(50, 260, 250, 50), new QuestKey(new Dictionary<string, int>() { { "InvisibleFatDragon", 2 } }, new List<string>() { "Invisible Scale", "Invisible Arm", "Invisible Sharpener", "Invisible Blade" }, "Hunt 2 Invisible Fat Dragons"));
            this.quests.Add(new Rectangle(50, 330, 250, 50), new QuestKey(new Dictionary<string, int>() { { "BawbGhais", 1 } }, new List<string>() { "Bawb Toenail", "Ghais Toenail", "Bawb Ghais Glue", "Bawb Feet", "Ghais Feet" }, "Hunt a BawbGhais"));

            // Load textures for background environments and calculate positions.
            this.backgroundTextureGrass = this.Content.Load<Texture2D>("Images/Map/BackgroundTextureGrass");
            this.backgroundTextureSnow = this.Content.Load<Texture2D>("Images/Map/BackgroundTextureSnow");
            this.backgroundTextureDesert = this.Content.Load<Texture2D>("Images/Map/BackgroundTextureDesert");
            this.backgroundTextureMetal = this.Content.Load<Texture2D>("Images/Map/BackgroundTextureMetal");

            this.grassBGx = -(this.backgroundTextureGrass.Width / 2);
            this.grassBGy = -(this.backgroundTextureGrass.Height / 2);

            this.UpperBaseY = this.grassBGy - this.backgroundTextureGrass.Bounds.Height;
            this.RightBaseX = this.grassBGx + this.backgroundTextureGrass.Bounds.Width;
            this.MiddleBaseX = this.grassBGx - this.backgroundTextureGrass.Bounds.Width;
            this.DownBaseY = this.grassBGy + this.backgroundTextureDesert.Bounds.Height;

            // Create a new SpriteBatch, which can be used to draw textures.
            this.spriteBatch = new SpriteBatch(this.GraphicsDevice);

            this.mainMenu = new MainMenu(this);
            this.gameMenu = new GameMenu(this);
            this.craftingMenu = new CraftingMenu(this);
            this.monsterMenu = new MonsterMenu(this);
            this.equipmentMenu = new EquipMenu(this);
            this.settingsMenu = new SettingsMenu(this);

            // Initialize buttons
            #region Direction
            InputButton directionLeft = new InputButton(this.Content.Load<Texture2D>("Images/Direction/Left")
                , new Rectangle(25, this.spriteBatch.GraphicsDevice.Viewport.Height - 95, 75, 75));
            this.inputManag.AddButton(directionLeft);

            InputButton directionRight = new InputButton(this.Content.Load<Texture2D>("Images/Direction/Right")
                , new Rectangle(25 + 75 + 75, this.spriteBatch.GraphicsDevice.Viewport.Height - 95, 75, 75));
            this.inputManag.AddButton(directionRight);

            InputButton directionUp = new InputButton(this.Content.Load<Texture2D>("Images/Direction/Up")
                , new Rectangle(25 + 75, this.spriteBatch.GraphicsDevice.Viewport.Height - 95 - 75, 75, 75));
            this.inputManag.AddButton(directionUp);

            InputButton directionDown = new InputButton(this.Content.Load<Texture2D>("Images/Direction/Down"),
                new Rectangle(25 + 75, this.spriteBatch.GraphicsDevice.Viewport.Height - 95, 75, 75));
            this.inputManag.AddButton(directionDown);
            #endregion

            #region Attacks
            InputButton Attack = new InputButton(this.Content.Load<Texture2D>("Images/IngameButtons/Attack"),
                new Rectangle(this.spriteBatch.GraphicsDevice.Viewport.Width - 90, this.spriteBatch.GraphicsDevice.Viewport.Height - 125, 80, 100));
            this.inputManag.AddButton(Attack);
            InputButton Skill = new InputButton(this.Content.Load<Texture2D>("Images/IngameButtons/Skill"),
                new Rectangle(this.spriteBatch.GraphicsDevice.Viewport.Width - 180, this.spriteBatch.GraphicsDevice.Viewport.Height - 125, 80, 100));
            this.inputManag.AddButton(Skill);
            #endregion

            // Initialize main player.
            mainplayer = new Player(this, this.Content.Load<Texture2D>("Images/Characters/Ack Joff/Left1"), new Vector2(0, 0), "Ack Joff"
                , "NoobSlasher", new Vector2(1, 1));

            // Initialize the camera.
            this.camera = new Camera2D(GraphicsDevice, mainplayer);
            this.camera.Initialize();
            this.entityManager = EntityManager.GetInstance(this.camera);

            this.entityManager.AddEntity(mainplayer);

            // Initialize indication bars for the main player.
            this.healthBar = new IndicationBar(mainplayer as Player, IndicationType.HealthPoints, this.Content.Load<Texture2D>("Images/Characters/HealthBar")
                , new Vector2(10, 10), 100, Color.Red);
            this.manaBar = new IndicationBar(mainplayer as Player, IndicationType.ManaPoints, this.Content.Load<Texture2D>("Images/Characters/ManaBar")
                , new Vector2(100, 10), 100, Color.Blue);

            // Add invisible walls to the entity manager, those are used to mark the end of the map.
            Entity upperWallLeft = new Wall("UpperWallLeft", this.Content.Load<Texture2D>("Images/Map/HorizontalEdgeWall"), new Vector2(this.MiddleBaseX
                , this.UpperBaseY - 50));
            this.entityManager.AddEntity(upperWallLeft);

            Entity upperWallMiddle = new Wall("UpperWallMiddle", this.Content.Load<Texture2D>("Images/Map/HorizontalEdgeWall"), new Vector2(this.grassBGx
                , this.UpperBaseY - 50));
            this.entityManager.AddEntity(upperWallMiddle);

            Entity UpperWallRight = new Wall("UpperWallRight", this.Content.Load<Texture2D>("Images/Map/HorizontalEdgeWall"), new Vector2(this.RightBaseX
                , this.UpperBaseY - 50));
            this.entityManager.AddEntity(UpperWallRight);

            Entity LowerWallLeft = new Wall("LowerWallLeft", this.Content.Load<Texture2D>("Images/Map/HorizontalEdgeWall"), new Vector2(this.MiddleBaseX
                , this.DownBaseY + this.backgroundTextureDesert.Bounds.Height));
            this.entityManager.AddEntity(LowerWallLeft);

            Entity LowerWallMiddle = new Wall("LowerWallMiddle", this.Content.Load<Texture2D>("Images/Map/HorizontalEdgeWall"), new Vector2(this.grassBGx
                , this.DownBaseY + this.backgroundTextureDesert.Bounds.Height));
            this.entityManager.AddEntity(LowerWallMiddle);

            Entity LowerWallRight = new Wall("LowerWallRight", this.Content.Load<Texture2D>("Images/Map/HorizontalEdgeWall"), new Vector2(this.RightBaseX
                , this.DownBaseY + this.backgroundTextureDesert.Bounds.Height));
            this.entityManager.AddEntity(LowerWallRight);

            Entity LeftVerticalWallUpper = new Wall("LeftVerticalWallUpper", this.Content.Load<Texture2D>("Images/Map/VerticalEdgeWall")
                , new Vector2(this.MiddleBaseX - 50, this.UpperBaseY));
            this.entityManager.AddEntity(LeftVerticalWallUpper);

            Entity LeftVerticalWallMiddle = new Wall("LeftVerticalWallMiddle", this.Content.Load<Texture2D>("Images/Map/VerticalEdgeWall"),
                new Vector2(this.MiddleBaseX - 50, this.grassBGy));
            this.entityManager.AddEntity(LeftVerticalWallMiddle);

            Entity LeftVerticalWallDown = new Wall("LeftVerticalWallDown", this.Content.Load<Texture2D>("Images/Map/VerticalEdgeWall"),
                new Vector2(this.MiddleBaseX - 50, this.DownBaseY));
            this.entityManager.AddEntity(LeftVerticalWallDown);

            Entity RightVerticalWallUpper = new Wall("RightVerticalWallUpper", this.Content.Load<Texture2D>("Images/Map/VerticalEdgeWall"),
                new Vector2((this.RightBaseX + this.backgroundTextureSnow.Bounds.Width) + 50, this.UpperBaseY));
            this.entityManager.AddEntity(RightVerticalWallUpper);

            Entity RightVerticalWallMiddle = new Wall("RightVerticalWallMiddle", this.Content.Load<Texture2D>("Images/Map/VerticalEdgeWall"),
                new Vector2((this.RightBaseX + this.backgroundTextureSnow.Bounds.Width) + 50, this.grassBGy));
            this.entityManager.AddEntity(RightVerticalWallMiddle);

            Entity RightVerticalWallDown = new Wall("RightVerticalWallDown", this.Content.Load<Texture2D>("Images/Map/VerticalEdgeWall"),
                new Vector2((this.RightBaseX + this.backgroundTextureSnow.Bounds.Width) + 50, this.DownBaseY));
            this.entityManager.AddEntity(RightVerticalWallDown);

            // Add random entities to the game.
            Entity pokemonTree = new PokemonTree("PokemonTree", this.Content.Load<Texture2D>("Images/Map/PokemonTree"), new Vector2(5, -200));
            this.entityManager.AddEntity(pokemonTree);

            Entity waterOnGrass = new WaterPool("Water1", this.Content.Load<Texture2D>("Images/Map/WaterTexture"), new Vector2(-880, 300));
            this.entityManager.AddEntity(waterOnGrass);

            Entity pokemonTree1 = new PokemonTree("PokemonTree1", this.Content.Load<Texture2D>("Images/Map/PokemonTree"), new Vector2(-400, -100));
            this.entityManager.AddEntity(pokemonTree1);

            Entity desertHut1 = new DesertHut("DesertHut", this.Content.Load<Texture2D>("Images/Map/DesertHut"), new Vector2(-2000, -100));
            this.entityManager.AddEntity(desertHut1);

            Entity desertruin1 = new DesertRuines("DesertRuins", this.Content.Load<Texture2D>("Images/Map/DesertRuines"), new Vector2(-2500, -800));
            this.entityManager.AddEntity(desertruin1);

            Entity desertRuin2 = new DesertRuines("DesertRuins1", this.Content.Load<Texture2D>("Images/Map/DesertRuinWood"), new Vector2(-2750, 400));
            this.entityManager.AddEntity(desertRuin2);

            Entity metalCilo1 = new MetalWorldEntity("MetalFluidCilo1", this.Content.Load<Texture2D>("Images/Map/MetalFluidCilo"), new Vector2(1000, 1000));
            this.entityManager.AddEntity(metalCilo1);

            Entity metalBowl1 = new MetalWorldEntity("MetalBowl1", this.Content.Load<Texture2D>("Images/Map/MetalBowlEntity"), new Vector2(0, 1500));
            this.entityManager.AddEntity(metalBowl1);

            Entity iceCaveWall1 = new IceWorldEntity("IceCaveWall1", this.Content.Load<Texture2D>("Images/Map/IceCaveWall"), new Vector2(1100, 0));
            this.entityManager.AddEntity(iceCaveWall1);

            this.saveFile = SaveFile.Instance;
        }

        public void loadSaveFile()
        {
            SaveFileData loadedData = new SaveFileData();
            this.saveFile.Load(ref loadedData);
            (this.mainplayer as Player).WeaponName = loadedData.WeaponName;

            // Load contents of save file into memory.
            var inventoryIterator = loadedData.Inventory.Items.GetEnumerator();
            while (inventoryIterator.MoveNext())
            {
                if (inventoryIterator.Current.Value > 1)
                {
                    int counter = 0;
                    while (counter < inventoryIterator.Current.Value)
                    {
                        this.inventory.AddItem(inventoryIterator.Current.Key);
                        ++counter;
                    }
                }
                else
                {
                    this.inventory.AddItem(inventoryIterator.Current.Key);
                }
            }

            var craftedIterator = loadedData.CraftedItems.GetEnumerator();
            while (craftedIterator.MoveNext())
            {
                if (craftedIterator.Current != "NoobSlasher")
                    this.equipWeapons.Add(craftedIterator.Current);
            }
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
            this.Content.Unload();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            this.menuSoundInstance.Volume = (float)Settings.settingVolume / 100;
            this.monsterSoundInstance.Volume = (float)Settings.settingVolume / 100;

            if (this.navLocation != "MonsterHunt" && this.menuSoundInstance.State != SoundState.Playing)
            {
                this.monsterSoundInstance.Stop();
                this.menuSoundInstance.Play();
            }
            else if (this.navLocation == "MonsterHunt" && this.monsterSoundInstance.State != SoundState.Playing)
            {
                this.menuSoundInstance.Stop();
                this.monsterSoundInstance.Play();
            }

            if (this.questClear)
            {
                Thread.Sleep(5000);
                this.navLocation = "GameMenu";
                this.questClear = false;
            }

            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            {
                switch (this.navLocation)
                {
                    case "MainMenu":
                        this.Exit();
                        break;
                    case "GameMenu":
                        // Save player data to file.
                        SaveFileData savedData = new SaveFileData();
                        savedData.WeaponName = (this.mainplayer as Player).WeaponName;
                        savedData.Inventory = this.inventory;
                        savedData.CraftedItems = this.equipWeapons;
                        this.saveFile.Save(ref savedData);
                        this.Exit();
                        break;
                    case "MonsterMenu":
                        this.navLocation = "GameMenu";
                        break;
                    case "MonsterHunt":
                        this.shouldEndGame = true;
                        break;
                    case "Equipment":
                        this.equipmentMenu.ReloadWeapons = true;
                        this.navLocation = "GameMenu";
                        break;
                    case "Crafting":
                        this.navLocation = "GameMenu";
                        break;
                    case "SettingsMenu":
                        this.navLocation = "GameMenu";
                        break;
                }
            }

            switch (this.navLocation)
            {
                case "MonsterMenu":
                    this.monsterMenu.Update(gameTime);
                    this.navLocation = this.monsterMenu.ShouldCloseMenu;
                    break;
                case "MonsterHunt":
                    // Update game objects.
                    this.entityManager.Update(gameTime);
                    this.aiManager.Update(gameTime);
                    this.healthBar.Update(gameTime);
                    this.manaBar.Update(gameTime);
                    this.camera.Update(gameTime);
                    break;
                case "MainMenu":
                    // Update main menu.
                    this.mainMenu.Update(gameTime);
                    this.navLocation = this.mainMenu.ShouldCloseMenu;
                    break;
                case "GameMenu":
                    this.gameMenu.Update(gameTime);
                    this.navLocation = this.gameMenu.ShouldCloseMenu;
                    break;
                case "Crafting":
                    this.craftingMenu.Update(gameTime);
                    this.navLocation = this.craftingMenu.ShouldCloseMenu;
                    break;
                case "Equipment":
                    this.equipmentMenu.Update(gameTime);
                    this.navLocation = this.equipmentMenu.ShouldCloseMenu;
                    break;
                case "SettingsMenu":
                    this.settingsMenu.Update(gameTime);
                    this.navLocation = this.settingsMenu.ShouldCloseMenu;
                    break;
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            //this.GraphicsDevice.Clear(Color.Blue);

            switch (this.navLocation)
            {
                case "MonsterHunt":
                    if (AIManager.GetInstance(this).Managedbots.Count > 0)
                    {
                        // Initialize spritebatch using camera transform
                        this.spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null
                            , this.camera.Transform);

                        // Draw first grass environment tile, middle.
                        if (this.camera.IsInView(new Rectangle((int)grassBGx, (int)grassBGy, this.backgroundTextureGrass.Bounds.Width
                            , this.backgroundTextureGrass.Bounds.Height)))
                        {
                            this.spriteBatch.Draw(this.backgroundTextureGrass, new Vector2(grassBGx, grassBGy), Color.White);
                        }

                        // Draw second grass environment tile, upper middle.
                        if (this.camera.IsInView(new Rectangle((int)grassBGx, (int)UpperBaseY, this.backgroundTextureGrass.Bounds.Width
                            , this.backgroundTextureGrass.Bounds.Height)))
                        {
                            this.spriteBatch.Draw(this.backgroundTextureGrass, new Vector2(grassBGx, UpperBaseY), Color.White);
                        }

                        // Draw first snow environment tile, middle right.
                        if (this.camera.IsInView(new Rectangle((int)RightBaseX, (int)grassBGy, this.backgroundTextureSnow.Bounds.Width
                            , this.backgroundTextureSnow.Bounds.Height)))
                        {
                            this.spriteBatch.Draw(this.backgroundTextureSnow, new Vector2(RightBaseX, grassBGy), Color.White);
                        }

                        // Draw second snow environment tile, upper right.
                        if (this.camera.IsInView(new Rectangle((int)RightBaseX, (int)UpperBaseY, this.backgroundTextureSnow.Bounds.Width
                            , this.backgroundTextureSnow.Bounds.Height)))
                        {
                            this.spriteBatch.Draw(this.backgroundTextureSnow, new Vector2(RightBaseX, UpperBaseY), Color.White);
                        }

                        // Draw first desert environment tile, middle left.
                        if (this.camera.IsInView(new Rectangle((int)MiddleBaseX, (int)grassBGy, this.backgroundTextureSnow.Bounds.Width
                            , this.backgroundTextureSnow.Bounds.Height)))
                        {
                            this.spriteBatch.Draw(this.backgroundTextureDesert, new Vector2(MiddleBaseX, grassBGy), Color.White);
                        }

                        // Draw the second desert environment tile, upper left.
                        if (this.camera.IsInView(new Rectangle((int)MiddleBaseX, (int)UpperBaseY, this.backgroundTextureDesert.Bounds.Width
                            , this.backgroundTextureDesert.Bounds.Height)))
                        {
                            this.spriteBatch.Draw(this.backgroundTextureDesert, new Vector2(MiddleBaseX, UpperBaseY), Color.White);
                        }

                        // Draw the third desert environment tile, left down.
                        if (this.camera.IsInView(new Rectangle((int)MiddleBaseX, (int)DownBaseY, this.backgroundTextureDesert.Bounds.Width
                            , this.backgroundTextureDesert.Bounds.Height)))
                        {
                            this.spriteBatch.Draw(this.backgroundTextureDesert, new Vector2(MiddleBaseX, DownBaseY), Color.White);
                        }

                        // Draw the first metal environment tile, middle down.
                        if (this.camera.IsInView(new Rectangle((int)this.grassBGx, (int)DownBaseY, this.backgroundTextureMetal.Bounds.Width
                            , this.backgroundTextureMetal.Bounds.Height)))
                        {
                            this.spriteBatch.Draw(this.backgroundTextureMetal, new Vector2(this.grassBGx, DownBaseY), Color.White);
                        }

                        // Draw the second metal environment tile, right down.
                        if (this.camera.IsInView(new Rectangle((int)RightBaseX, (int)DownBaseY, this.backgroundTextureMetal.Bounds.Width
                            , this.backgroundTextureMetal.Bounds.Height)))
                        {
                            this.spriteBatch.Draw(this.backgroundTextureMetal, new Vector2(RightBaseX, DownBaseY), Color.White);
                        }

                        // If the game should end because of player death, display a quest failed message and loop back to menu.
                        if (this.shouldEndGame)
                        {
                            this.monsterMenu.CurrentStartedQuest = null;

                            if (this.failtime > 0)
                            {
                                this.spriteBatch.Draw(this.Content.Load<Texture2D>("Images/FailCircle"), new Vector2(this.mainplayer.Position.X - 240, this.mainplayer.Position.Y - 240), Color.White);
                                --this.failtime;
                            }
                            else
                            {
                                this.failtime = 75;
                                this.navLocation = "GameMenu";
                                this.shouldEndGame = false;
                                this.MainPlayer.resetPlayer();
                            }
                        }

                        // Draw game objects.
                        this.entityManager.Draw(this.spriteBatch);

                        this.spriteBatch.End();

                        // New spritebatch to draw buttons on screen
                        this.spriteBatch.Begin();
                        this.inputManag.Draw(this.spriteBatch);
                        this.healthBar.Draw(spriteBatch);
                        this.manaBar.Draw(spriteBatch);
                        this.spriteBatch.End();
                    }
                    else
                    {
                        this.spriteBatch.Begin();
                        //Draw quest clear stuff
                        spriteBatch.DrawString(this.Font, "Quest Clear!", new Vector2(50, 35), Color.White, 0f, new Vector2(0, 0), 1.3f, SpriteEffects.None, 0f);
                        spriteBatch.DrawString(this.Font, "Items gained:", new Vector2(50, 70), Color.White, 0f, new Vector2(0, 0), 1.1f, SpriteEffects.None, 0f);
                        if (this.currentQuestRewards != null && this.currentQuestRewards.Count > 0)
                        {
                            questClear = true;
                            Random random = new Random();
                            int spacing = 20;

                            for (int i = 0; this.currentQuestRewards.Count > i; i++)
                            {
                                int luckyNumber = random.Next(0, 4);
                                spriteBatch.DrawString(this.Font, luckyNumber + "X - " + this.currentQuestRewards[i], new Vector2(50, 70 + spacing), Color.White, 0f, new Vector2(0, 0), 1.1f, SpriteEffects.None, 0f);
                                for (int j = 1; luckyNumber >= j; j++)
                                {
                                    inventory.AddItem(this.currentQuestRewards[i]);
                                }
                                spacing += 20;
                            }

                            // Reset AI and player position.
                            this.mainplayer.Position = new Vector2(0, 0);
                            this.aiManager.ClearAI();
                            this.monsterMenu.CurrentStartedQuest = null;
                        }
                        else
                        {
                            spriteBatch.DrawString(this.Font, "None! D:<", new Vector2(50, 70), Color.White, 0f, new Vector2(0, 0), 1.0f, SpriteEffects.None, 0f);
                        }
                        this.spriteBatch.End();
                    }
                    break;
                case "MainMenu":
                    // Draw main menu
                    this.spriteBatch.Begin();
                    if (!this.backgroundMusic)
                    {
                        this.spriteBatch.DrawString(this.Font, "* Background music has been disabled because of the media player", new Vector2(20, 400), Color.Black, 0f, new Vector2(0, 0), 1.3f, SpriteEffects.None, 0f);
                        this.spriteBatch.DrawString(this.Font, "* Background music can be adjusted in the settings menu", new Vector2(20, 430), Color.Black, 0f, new Vector2(0, 0), 1.3f, SpriteEffects.None, 0f);
                    }
                    this.spriteBatch.End();
                    this.mainMenu.Draw(this.spriteBatch);
                    break;
                case "GameMenu":
                    this.shouldEndGame = false;
                    this.gameMenu.Draw(this.spriteBatch);
                    break;
                case "MonsterMenu":
                    this.shouldEndGame = false;
                    this.monsterMenu.Draw(this.spriteBatch);
                    break;
                case "Crafting":
                    this.craftingMenu.Draw(this.spriteBatch);
                    break;
                case "Equipment":
                    this.equipmentMenu.Draw(this.spriteBatch);
                    break;
                case "SettingsMenu":
                    this.settingsMenu.Draw(this.spriteBatch);
                    break;
            }

            base.Draw(gameTime);
        }

        /// <summary>
        /// Gets the input controller that is associated with the game class.
        /// </summary>
        public InputControllerManager GameInputController
        {
            get
            {
                return this.inputManag;
            }
        }

        /// <summary>
        /// Gets font
        /// </summary>
        public SpriteFont Font
        {
            get
            {
                return this.spriteFont;
            }
        }

        /// <summary>
        /// Indicates whether the main menu is currently loaded or should be loaded on the next game update.
        /// </summary>
        public string NavLocation
        {
            get
            {
                return this.navLocation;
            }
        }

        /// <summary>
        /// Gets or sets whether the game should end, because of the player that has fainted.
        /// </summary>
        public bool ShouldEndGame
        {
            get
            {
                return this.shouldEndGame;
            }
            set { this.shouldEndGame = value; }
        }

        /// <summary>
        /// Gets the main player. Needed for inventory access.
        /// </summary>
        public Player MainPlayer
        {
            get { return this.mainplayer as Player; }
        }

        /// <summary>
        /// Gets the inventory associated to the player.
        /// </summary>
        public Inventory Inventory
        {
            get { return this.inventory; }
        }

        /// <summary>
        /// Gets the list of weapons that are currently available for equip.
        /// </summary>
        public List<string> EquipWeapons
        {
            get { return equipWeapons; }
        }

        /// <summary>
        /// Adds a weapon to the list of crafted weapons.
        /// </summary>
        /// <param name="weaponName">The name of the newly added weapon.</param>
        public void addWeapon(string weaponName)
        {
            if (!this.equipWeapons.Contains(weaponName))
                this.equipWeapons.Add(weaponName);
        }

        /// <summary>
        /// Gets the input control manager to retrieve key presses globally.
        /// </summary>
        public InputControllerManager InputManag
        {
            get { return inputManag; }
        }

        /// <summary>
        /// Gets the quests currently available.
        /// </summary>
        public Dictionary<Rectangle, QuestKey> Quests
        {
            get { return quests; }
        }

        /// <summary>
        /// Gets or sets the current quest rewards for when the current quest is completed.
        /// </summary>
        public List<string> CurrentQuestRewards
        {
            get { return currentQuestRewards; }
            set { currentQuestRewards = value; }
        }

        /// <summary>
        /// Gets the referrence to the savefile currently loaded.
        /// </summary>
        public SaveFile SaveFile
        {
            get { return this.saveFile; }
        }

        public MonsterMenu MonsterMenu
        {
            get { return monsterMenu; }
        }
    }
}
