﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Monhun.Entities;
using Microsoft.Xna.Framework.Audio;

namespace Monhun.Skills
{
    /// <summary>
    /// Represents the fart escape skill. Can be cast by the main player as a skill.
    /// </summary>
    public sealed class FartEscape : Skill
    {
        public FartEscape(GameMain game)
            : base(game)
        {
            base.isSkillLocationSet = false;
        }

        /// <summary>
        /// Casts the skill with the specified player as damage owner.
        /// </summary>
        /// <param name="positionFrom">The position to cast the skill from.</param>
        /// <param name="Facing">The face the player is currently to.</param>
        /// <returns>The position the skill will move to from the starting position.</returns>
        public override Vector2 Cast(Vector2 positionFrom, FacingDirection Facing, string name, Texture2D texture)
        {
            Entity FEE = new FartEscapeEntity(base.game, Facing, 75, name, texture, positionFrom);
            EntityManager.GetInstance(null).AddEntity(FEE);

            switch (Facing)
            {
                case FacingDirection.Up:
                    return new Vector2(positionFrom.X, positionFrom.Y - 100);
                case FacingDirection.Down:
                    return new Vector2(positionFrom.X, positionFrom.Y + 100);
                case FacingDirection.Left:
                    return new Vector2(positionFrom.X - 100, positionFrom.Y);
                case FacingDirection.Right:
                    return new Vector2(positionFrom.X + 100, positionFrom.Y);
            }

            return new Vector2(positionFrom.X, positionFrom.Y);
        }
    }
}