using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Monhun.Entities;


namespace Monhun.Skills
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class FartEscapeEntity : Monhun.Entities.Entity
    {
        private Vector2 skillLocation;
        private FacingDirection skillDirection;
        private int skillDuration;
        private Texture2D texture;
        private string skillName;

        public FartEscapeEntity(FacingDirection Facing, int skillDuration, string name, Texture2D texture, Vector2 position)
            : base(name, texture, position)
        {
            this.skillLocation = position;
            this.skillDirection = Facing;
            this.texture = texture;
            this.skillDuration = skillDuration;
            this.skillName = name;
        }

        public override bool IsMoveable
        {
            get { return true; }
        }

        public override void Update(GameTime gameTime)
        {
            
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            switch (this.skillDirection)
            {
                case FacingDirection.Up:
                    this.skillLocation.Y += 2;
                    break;
                case FacingDirection.Down:
                    this.skillLocation.Y -= 2;
                    break;
                case FacingDirection.Left:
                    this.skillLocation.X += 2;
                    break;
                case FacingDirection.Right:
                    this.skillLocation.X -= 2;
                    break;
            }

            spriteBatch.Draw(this.texture, this.skillLocation, null, Color.White, 0f, Vector2.Zero, 2, SpriteEffects.None, 0f);

            this.skillDuration--;
            if (0 >= skillDuration)
            {
                EntityManager.Instance.RemoveEntity(this.skillName);
            }
        }
    }
}
