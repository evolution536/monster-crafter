using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Monhun.Entities;

namespace Monhun.Skills
{
    /// <summary>
    /// Represents a skill that can be cast by the main player. This class is abstract and cannot be instantiated.
    /// </summary>
    public abstract class Skill
    {
        protected GameMain game;
        protected bool isSkillLocationSet;

        protected Texture2D skillTextureUp;
        protected Texture2D skillTextureDown;
        protected Texture2D skillTextureLeft;
        protected Texture2D skillTextureRight; 
        protected Vector2 skillLocation;
        protected FacingDirection skillDirection;
        protected Entity collisionAgainst;

        public Skill(GameMain game)
        {
            this.game = game;
            this.skillTextureUp = game.Content.Load<Texture2D>("Images/Skills/FartUp");
            this.skillTextureDown = game.Content.Load<Texture2D>("Images/Skills/FartDown");
            this.skillTextureLeft = game.Content.Load<Texture2D>("Images/Skills/FartLeft");
            this.skillTextureRight = game.Content.Load<Texture2D>("Images/Skills/FartRight");
        }

        /// <summary>
        /// Clears the skill location so the skill cannot be cast while resetted.
        /// </summary>
        public void Clear()
        {
            this.isSkillLocationSet = false;
        }

        /// <summary>
        /// Casts the skill with the specified player as damage owner.
        /// </summary>
        /// <param name="positionFrom">The position to cast the skill from.</param>
        /// <param name="Facing">The face the player is currently to.</param>
        /// <returns>The position the skill will move to from the starting position.</returns>
        public abstract Vector2 Cast(Vector2 positionFrom, FacingDirection Facing, string name, Texture2D texture);

        /// <summary>
        /// Draws the skill's associated textures to the screen.
        /// </summary>
        /// <param name="batch">The spritebatch that is currently active for drawing.</param>
        /// <param name="Side">The texture that will be drawn.</param>
        public void Draw(SpriteBatch batch, Texture2D Side)
        {
            batch.Draw(Side, skillLocation, null, Color.White, 0f, Vector2.Zero, 2, SpriteEffects.None, 0f);
        }

        public Entity CollisionAgainst;
    }
}