﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Monhun.Entities;
using Microsoft.Xna.Framework.Audio;

namespace Monhun.Skills
{
    /// <summary>
    /// Represents the fireball skill. Can be cast by the main player as a skill.
    /// </summary>
    /*public sealed class Fireball : Skill
    {
        public Fireball(Texture2D textureUp, Texture2D textureDown, Texture2D textureLeft, Texture2D textureRight, SoundEffect sound)
            : base(textureUp, textureDown, textureLeft, textureRight, sound)
        {

        }

        /// <summary>
        /// Casts the skill with the specified player as damage owner.
        /// </summary>
        /// <param name="positionFrom">The position to cast the skill from.</param>
        /// <param name="batch">The spritebatch used to draw any skill related textures.</param>
        /// <param name="Facing">The face the player is currently to.</param>
        /// <returns>The position the skill will move to from the starting position.</returns>
        public override Vector2 Cast(Vector2 positionFrom, SpriteBatch batch, FacingDirection Facing)
        {
            throw new NotImplementedException();
        }
    }*/
}