using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Monhun.Entities;

namespace Monhun.Skills
{
    /// <summary>
    /// Represents a skill that can be cast by the main player. This class is abstract and cannot be instantiated.
    /// </summary>
    public abstract class Skill
    {
        public Texture2D skillTextureUp;
        public Texture2D skillTextureDown;
        public Texture2D skillTextureLeft;
        public Texture2D skillTextureRight;
        protected bool isSkillLocationSet;
        protected Vector2 skillLocation;
        public FacingDirection skillDirection;

        /// <summary>
        /// Clears the skill location so the skill cannot be cast while resetted.
        /// </summary>
        public void Clear()
        {
            this.isSkillLocationSet = false;
        }

        /// <summary>
        /// Casts the skill with the specified player as damage owner.
        /// </summary>
        /// <param name="positionFrom">The position to cast the skill from.</param>
        /// <param name="batch">The spritebatch used to draw any skill related textures.</param>
        /// <param name="Facing">The face the player is currently to.</param>
        /// <returns>The position the skill will move to from the starting position.</returns>
        public abstract Vector2 Cast(Vector2 positionFrom, SpriteBatch batch, FacingDirection Facing);

        /// <summary>
        /// Draws the skill's associated textures to the screen.
        /// </summary>
        /// <param name="batch">The spritebatch that is currently active for drawing.</param>
        /// <param name="Side">The texture that will be drawn.</param>
        public void Draw(SpriteBatch batch, Texture2D Side)
        {
            batch.Draw(Side, skillLocation, null, Color.White, 0f, Vector2.Zero, 2, SpriteEffects.None, 0f);
        }
    }
}
