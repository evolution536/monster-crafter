﻿using System;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Collections.Generic;
using Monhun.Entities;
using Monhun.Weapons;

namespace Monhun
{
    /// <summary>
    /// Structure that contains the data the savefile stores and loads.
    /// Defined as class because it will be passed as parameter, which needs a pointer, obviously.
    /// </summary>
    public class SaveFileData
    {
        string player;
        Inventory inventory;
        List<string> craftedItems;

        /// <summary>
        /// Gets the player in the save file.
        /// </summary>
        public string WeaponName { get { return this.player; } set { this.player = value; } }

        /// <summary>
        /// Gets the inventory saved in the save file.
        /// </summary>
        public Inventory Inventory { get { return this.inventory; } set { this.inventory = value; } }

        /// <summary>
        /// Gets the list of crafted items in the save file.
        /// </summary>
        public List<string> CraftedItems { get { return this.craftedItems; } set { this.craftedItems = value; } }

        public SaveFileData(string player, Inventory inventory, List<string> craftedItems)
        {
            this.player = player;
            this.inventory = inventory;
            this.craftedItems = craftedItems;
        }

        public SaveFileData()
        {
            this.player = null;
            this.inventory = new Inventory();
            this.craftedItems = new List<string>();
        }
    }

    /// <summary>
    /// Represents a savefile that contains progress information from the player.
    /// </summary>
    public sealed class SaveFile
    {
        const string FILENAME = "MonCraftSaveFile.xml";
        static SaveFile instance;

        IsolatedStorageFile mFile;
        XDocument mDocument;

        /// <summary>
        /// Initializes a new instance of the SaveFile class.
        /// </summary>
        SaveFile()
        {
            this.mFile = IsolatedStorageFile.GetUserStoreForApplication();
            this.mDocument = this.CreateDocument();
        }

        /// <summary>
        /// Returns the singleton instance of SaveFile.
        /// </summary>
        public static SaveFile Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SaveFile();
                }

                return instance;
            }
        }

        private IsolatedStorageFileStream GetStream()
        {
            return mFile.OpenFile(FILENAME, FileMode.OpenOrCreate, FileAccess.ReadWrite
                    , FileShare.ReadWrite);
        }

        private bool DatastoreExists
        {
            get { return mFile.FileExists(FILENAME) ? true : false; }
        }

        private XDocument CreateDocument()
        {
            IsolatedStorageFileStream mFileStream = GetStream();
            XDocument doc;
            if (!DatastoreExists || mFileStream.Length <= 0)
            {
                doc = new XDocument(new XDeclaration("1.0", "utf-8", ""), new XElement("MonCraft"));
                doc.Save(mFileStream, SaveOptions.DisableFormatting);
                return doc;
            }
            doc = XDocument.Load(mFileStream);
            mFileStream.Close();
            return doc;
        }

        /// <summary>
        /// Loads the savefile from the isolated store into memory.
        /// <param name="dataStructure">The data structure into which the saved data will be loaded.</param>
        /// </summary>
        public void Load(ref SaveFileData dataStructure)
        {
            try
            {
                if (this.mDocument == null)
                {
                    this.mDocument = this.CreateDocument();
                }

                XElement mPlayerElement = this.mDocument.Element("MonCraft");
                dataStructure.WeaponName = mPlayerElement.Element("MainPlayer").Element("WeaponName").Value;

                XElement mInventoryElement = this.mDocument.Element("MonCraft").Element("Inventory");
                var inventoryItems = (from item in mInventoryElement.Elements("InventoryItem") select item).ToArray();
                for (int i = 0; i < inventoryItems.Count(); i++)
                {
                    int itemCount = int.Parse(inventoryItems[i].Attribute("Count").Value);
                    if (itemCount > 1)
                    {
                        int counter = 0;
                        while (counter < itemCount)
                        {
                            dataStructure.Inventory.AddItem(inventoryItems[i].Attribute("Name").Value);
                            ++counter;
                        }
                    }
                    else
                    {
                        dataStructure.Inventory.AddItem(inventoryItems[i].Attribute("Name").Value);
                    }
                }

                XElement craftedElement = this.mDocument.Element("MonCraft").Element("CraftedItems");
                var craftedItems = (from item in craftedElement.Elements("CraftedItem") select item).ToArray();

                for (int i = 0; i < craftedItems.Count(); i++)
                {
                    dataStructure.CraftedItems.Add(craftedItems[i].Value);
                }
            }
            catch (NullReferenceException)
            {
                
            }
        }

        /// <summary>
        /// Flushes the savefile to the isolated storage.
        /// <param name="data">The data to write to the savefile.</param>
        /// </summary>
        public void Save(ref SaveFileData data)
        {
            this.DeleteSaveFile();
            this.mDocument = this.CreateDocument();

            IsolatedStorageFileStream mFileStream = this.mFile.OpenFile(FILENAME, FileMode.OpenOrCreate, FileAccess.ReadWrite
                , FileShare.ReadWrite);

            // Remove all nodes from the XML document in memory.
            mDocument.Root.Nodes().Remove();

            // Re-add all nessecary nodes to the XML document in memory.
            string weaponName = data.WeaponName == null ? "NoobSlasher" : data.WeaponName;
            mDocument.Root.Add(new XElement("MainPlayer", new XElement("WeaponName", weaponName)));
            XElement inventoryNode = new XElement("Inventory");

            var iterator = data.Inventory.Items.GetEnumerator();
            while (iterator.MoveNext())
            {
                if (iterator.Current.Value > 0 && iterator.Current.Key != null)
                    inventoryNode.Add(new XElement("InventoryItem", new XAttribute("Name", iterator.Current.Key), new XAttribute("Count", iterator.Current.Value)));
            }

            mDocument.Root.Add(inventoryNode);

            XElement craftedItemsNode = new XElement("CraftedItems");
            var iterator2 = data.CraftedItems.GetEnumerator();
            while (iterator2.MoveNext())
            {
                if(iterator2.Current != null)
                    craftedItemsNode.Add(new XElement("CraftedItem", iterator2.Current));
            }

            mDocument.Root.Add(craftedItemsNode);

            // Write-out memory buffer and close stream.
            mDocument.Save(mFileStream, SaveOptions.DisableFormatting);
            mFileStream.Close();
        }

        /// <summary>
        /// Deletes the savefile from the isolated store. This action can not be undone.
        /// </summary>
        public void DeleteSaveFile()
        {
            if (this.DatastoreExists)
            {
                this.mFile.DeleteFile(FILENAME);
                this.mDocument = null;
            }
        }
    }
}