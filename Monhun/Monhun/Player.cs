﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;
using Monhun.Skills;
using Monhun.Weapons;

namespace Monhun.Entities
{
    /// <summary>
    /// Indicates which direction the player face currently is pointing to.
    /// </summary>
    public enum FacingDirection
    {
        /// <summary>
        /// Indicates that the player is facing left.
        /// </summary>
        Left,

        /// <summary>
        /// Indicates that the player is facing up.
        /// </summary>
        Up,

        /// <summary>
        /// Indicates that the player is facing right.
        /// </summary>
        Right,

        /// <summary>
        /// Indicates that the player is facing down.
        /// </summary>
        Down
    };

    /// <summary>
    /// Indicates whether the main player is currently using normal attacks or skills.
    /// </summary>
    public enum AttackOrSkill
    {
        /// <summary>
        /// Indicates that the main player is currently using normal attacks.
        /// </summary>
        Attack,

        /// <summary>
        /// Indicates that the main player is currently using skills.
        /// </summary>
        Skill,

        /// <summary>
        /// Blocks other skills or attacks when one is used
        /// </summary>
        Unlock
    };

    /// <summary>
    /// Represents the main player in the game.
    /// </summary>
    public sealed class Player : Entity
    {
        Vector2 velocity;
        GameMain game;

        Weapon weapon;
        float healthPoints;
        float manaPoints;
        int stepCount = 100;
        String skillShout;
        int skillShoutDuration = 75;
        int skillShoutTime;
        Skill[] playerSkills;
        FacingDirection facing;
        AttackOrSkill typeAttack;

        public Player(GameMain game, Texture2D texture, Vector2 center, string name, Weapon weapon, Vector2 velocity, Skill[] playerSkills)
            : base(name ,texture, center)
        {
            this.game = game;
            this.weapon = weapon;
            this.velocity = velocity;

            this.healthPoints = 100;
            this.manaPoints = 100;
            this.skillShoutTime = this.skillShoutDuration;
            this.playerSkills = playerSkills;
            this.typeAttack = AttackOrSkill.Unlock;
        }

        /// <summary>
        /// Indicates that a player is a moveable object. Needed for proper collision detection.
        /// </summary>
        public override bool IsMoveable
        {
            get { return true; }
        }

        /// <summary>
        /// Updates the player, correcting its position using pressed keys.
        /// </summary>
        public override void Update(GameTime gameTime)
        {
            foreach (TouchLocation tl in TouchPanel.GetState())
            {
                if ((tl.State == TouchLocationState.Pressed) || (tl.State == TouchLocationState.Moved))
                {
                    Vector2 newPos = new Vector2(tl.Position.X, tl.Position.Y);

                    if (game.GameInputController[0].Bounds.Contains((int)newPos.X, (int)newPos.Y))
                    {
                        this.virtualPosition.X -= (float)gameTime.ElapsedGameTime.Milliseconds / 10.0f * 1.0f;
                        if (stepCount < 400)
                        {
                            this.texture = this.game.Content.Load<Texture2D>("Images/Characters/Ack Joff/Left" + stepCount.ToString().Substring(0, 1));
                            stepCount += 20;
                        }
                        else
                        {
                            this.texture = this.game.Content.Load<Texture2D>("Images/Characters/Ack Joff/Left2");
                            stepCount = 100;
                        }
                        this.facing = FacingDirection.Left;
                    }
                    else if (game.GameInputController[1].Bounds.Contains((int)newPos.X, (int)newPos.Y))
                    {
                        this.virtualPosition.X += (float)gameTime.ElapsedGameTime.Milliseconds / 10.0f * 1.0f;
                        if (stepCount < 400)
                        {
                            this.texture = this.game.Content.Load<Texture2D>("Images/Characters/Ack Joff/Right" + stepCount.ToString().Substring(0, 1));
                            stepCount += 20;
                        }
                        else
                        {
                            this.texture = this.game.Content.Load<Texture2D>("Images/Characters/Ack Joff/Right2");
                            stepCount = 100;
                        }
                        this.facing = FacingDirection.Right;
                    }
                    else if (game.GameInputController[2].Bounds.Contains((int)newPos.X, (int)newPos.Y))
                    {
                        this.virtualPosition.Y -= (float)gameTime.ElapsedGameTime.Milliseconds / 10.0f * 1.0f;
                        if (stepCount < 400)
                        {
                            this.texture = this.game.Content.Load<Texture2D>("Images/Characters/Ack Joff/Up" + stepCount.ToString().Substring(0, 1));
                            stepCount += 20;
                        }
                        else
                        {
                            this.texture = this.game.Content.Load<Texture2D>("Images/Characters/Ack Joff/Up2");
                            stepCount = 100;
                        }
                        this.facing = FacingDirection.Up;
                    }
                    else if (game.GameInputController[3].Bounds.Contains((int)newPos.X, (int)newPos.Y))
                    {
                        this.virtualPosition.Y += (float)gameTime.ElapsedGameTime.Milliseconds / 10.0f * 1.0f;
                        if (stepCount < 400)
                        {
                            this.texture = this.game.Content.Load<Texture2D>("Images/Characters/Ack Joff/Down" + stepCount.ToString().Substring(0, 1));
                            stepCount += 20;
                        }
                        else
                        {
                            this.texture = this.game.Content.Load<Texture2D>("Images/Characters/Ack Joff/Down2");
                            stepCount = 100;
                        }
                        this.facing = FacingDirection.Down;
                    }

                    if (game.GameInputController[4].Bounds.Contains((int)newPos.X, (int)newPos.Y))
                    {
                        //Attack btn
                        if (typeAttack == AttackOrSkill.Unlock)
                        {
                            skillShoutTime = 25;
                            skillShout = "Secretly stab his mom attack!";
                            typeAttack = AttackOrSkill.Attack;
                        }
                    }
                    else if (game.GameInputController[5].Bounds.Contains((int)newPos.X, (int)newPos.Y))
                    {
                        //Skill btn
                        if (typeAttack == AttackOrSkill.Unlock)
                        {
                            skillShout = "Blind Black Man Beam";
                            typeAttack = AttackOrSkill.Skill;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Draws the player.
        /// </summary>
        public override void Draw(SpriteBatch batch)
        {
            batch.Draw(this.texture, this.position, null, Color.White, 0f, Vector2.Zero, 2, SpriteEffects.None, 0f);
            /* Normal attack */
            if (skillShoutTime > 0 && !String.IsNullOrEmpty(skillShout) && typeAttack == AttackOrSkill.Attack)
            {
                batch.DrawString(this.game.Font, skillShout, new Vector2(this.position.X - 30, this.position.Y - 30), Color.Black);
                skillShoutTime--;
            }
            if (skillShoutTime <= 0)
            {
                skillShout = null;
                skillShoutTime = skillShoutDuration;
                typeAttack = AttackOrSkill.Unlock;
            }
            /* ------ */

            /* Skills */
            if (skillShoutTime > 0 && !String.IsNullOrEmpty(skillShout) && typeAttack == AttackOrSkill.Skill)
            {
                batch.DrawString(this.game.Font, skillShout, new Vector2(this.position.X - 30, this.position.Y - 30), Color.Black);
                this.position = playerSkills[0].Cast(this.position, batch, this.facing);
                skillShoutTime--;
            }
            if (skillShoutTime <= 0)
            {
                skillShout = null;
                skillShoutTime = skillShoutDuration;
                typeAttack = AttackOrSkill.Unlock;
                playerSkills[0].Clear();
            }
            /* ------ */
        }

        /// <summary>
        /// Gets or sets the amount of health points the player currently has.
        /// </summary>
        public float HealthPoints
        {
            get
            {
                return this.healthPoints;
            }
            set
            {
                this.healthPoints = value;
            }
        }

        /// <summary>
        /// Gets or sets the amount of mana points the player currently has.
        /// </summary>
        public float ManaPoints
        {
            get
            {
                return this.manaPoints;
            }
            set
            {
                this.manaPoints = value;
            }
        }
    }
}