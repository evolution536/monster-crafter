using System;
using System.Collections.Generic;
using System.Linq;

namespace Monhun.Weapons
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Crafting
    {
        private Dictionary<string, Dictionary<string, int>> craftables = new Dictionary<string, Dictionary<string, int>>();

        public Crafting()
        {
            craftables.Add("Potatis poker", new Dictionary<string, int>() {{ "Potatis tail", 3 }, {"Potatis feet", 2}});
            craftables.Add("Bus slasher", new Dictionary<string, int>() { { "Flying Tail", 3 }, { "Flying feet", 2 }});
        }
    }
}
