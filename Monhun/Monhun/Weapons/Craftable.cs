﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Monhun.Weapons
{
    /// <summary>
    /// Represents a craftable that can be crafted using one or more items.
    /// </summary>
    public class Craftable : IDrawable
    {
        string name;
        Texture2D texture;
        int damage;
        Dictionary<string, int> itemsNeeded;

        /// <summary>
        /// Gets the needed items to craft this craftable.
        /// </summary>
        public Dictionary<string, int> ItemsNeeded
        {
            get { return itemsNeeded; }
        }

        /// <summary>
        /// Initializes a new instance of the craftable class.
        /// <param name="name">The name of the craftable.</param>
        /// </summary>
        public Craftable(string name, Texture2D texture, int damage, Dictionary<string, int> itemsNeeded)
        {
            this.name = name;
            this.texture = texture;
            this.damage = damage;
            this.itemsNeeded = itemsNeeded;
        }

        /// <summary>
        /// Gets the name of the craftable.
        /// </summary>
        public string Name
        {
            get { return this.name; }
        }

        /// <summary>
        /// Gets the damage of the craftable.
        /// </summary>
        public int Damage
        {
            get { return this.damage; }
        }

        /// <summary>
        /// Gets the texture associated with this craftable.
        /// </summary>
        public Texture2D Texture
        {
            get { return this.texture; }
        }

        /// <summary>
        /// Draws the craftable.
        /// </summary>
        /// <param name="spriteBatch">The spritebatch parameter from the main class.</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(this.texture, new Vector2(0, 0), Color.White);
        }
    }
}