﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework.Graphics;
using Monhun.Entities;
using Microsoft.Xna.Framework;

namespace Monhun.Weapons
{
    /// <summary>
    /// Represents a weapon that can be yielded by the main player.
    /// </summary>
    public class Weapon
    {
        static Weapon instance;
        protected GameMain game;
        Dictionary<string, Weapon> Weapons = new Dictionary<string, Weapon>();
        int damage;
        string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int Damage
        {
            get { return damage; }
            set { this.damage = value; }
        }

        public Weapon getWeapon(string WeaponName)
        {
            Weapon returnWeapon;
            if (Weapons.TryGetValue(WeaponName, out returnWeapon))
                return returnWeapon;

            return Weapons.First().Value;
        }

        public static Weapon GetInstance(GameMain game)
        {
            if (instance == null)
            {
                instance = new Weapon();
                instance.game = game;
                instance.loadWeapon();
            }

            return instance;
        }

        //Initialize weapons
        public void loadWeapon()
        {
            Weapons.Add("NoobSlasher", new LongSword(this.game, 5, "NoobSlasher"));
            Weapons.Add("PotatisPoker", new LongSword(this.game, 10, "PotatisPoker"));
            Weapons.Add("BusSlasher", new LongSword(this.game, 15, "BusSlasher"));
            Weapons.Add("RedGreenRandomStabber", new LongSword(this.game, 20, "RedGreenRandomStabber"));
            Weapons.Add("InvisiblePoker", new LongSword(this.game, 25, "InvisiblePoker"));
            Weapons.Add("BawbGhaisToenail", new LongSword(this.game, 30, "BawbGhaisToenail"));
        }
    }

    public sealed class GreatSword : Weapon
    {

    }

    public sealed class LongSword : Weapon
    {
        public LongSword(GameMain game, int damage, string name) : base()
        {
            base.game = game;
            base.Damage = damage;
            base.Name = name;
        }

        public void attackEnemy(FacingDirection facing, string weaponName, Texture2D attackAnimation, Vector2 attackLocation)
        {
            Entity attack = new WeaponAttack(base.game, "WeaponAttack", attackAnimation, attackLocation, facing, weaponName, 10, base.Damage);
            EntityManager.GetInstance(null).AddEntity(attack);
        }
    }

    public sealed class SwordAndShield : Weapon
    {

    }
}