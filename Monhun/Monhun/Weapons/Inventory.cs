using System;
using System.Collections.Generic;
using System.Linq;

namespace Monhun.Weapons
{
    /// <summary>
    /// Represents the inventory of the player. Contains all looted items needed for crafting.
    /// </summary>
    public sealed class Inventory
    {
        Dictionary<string, int> items;

        public Dictionary<string, int> Items
        {
            get { return items; }
        }

        /// <summary>
        /// Initializes a new instance of the Inventory class.
        /// </summary>
        public Inventory()
        {
            this.items = new Dictionary<string, int>();
        }

        /// <summary>
        /// Adds a new looted item to the inventory.
        /// </summary>
        /// <param name="item">The name of the item. If it is null, the function returns without action.</param>
        public void AddItem(string item)
        {
            if (!string.IsNullOrEmpty(item))
            {
                if (this.items.ContainsKey(item))
                {
                    // Item exists, increment amount in inventory.
                    ++this.items[item];
                }
                else
                {
                    // Item doesn't exist. Completely add to inventory.
                    this.items.Add(item, 1);
                }
            }
        }

        /// <summary>
        /// Removes an item from the inventory. If more than one of an item is present, the amount is decremented.
        /// If the item is not found in the inventory, nothing is done.
        /// </summary>
        /// <param name="item">The name of the item. If it is null, the function returns without action.</param>
        public void RemoveItem(string item)
        {
            if (!string.IsNullOrEmpty(item))
            {
                if (this.items.ContainsKey(item))
                {
                    // Item is found, check it's amount.
                    if (this.items[item] > 1)
                    {
                        // Amount is greater than 1, decrement amount.
                        --this.items[item];
                    }
                    else
                    {
                        // Only one piece of this item is present, remove the item completely.
                        this.items.Remove(item);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the amount of the specified item or sets the amount of the specified item. If the item does not exist, nothing is done
        /// and in case of get: -1 is returned.
        /// </summary>
        /// <param name="name">The name of the item. If the name is null, -1 is returned or nothing is done.</param>
        /// <returns>The amount of the specified item that is present in the inventory.</returns>
        public int this[string name]
        {
            get
            {
                return !string.IsNullOrEmpty(name) && this.items.ContainsKey(name) ? this.items[name] : -1;
            }
            set
            {
                if (!string.IsNullOrEmpty(name) && this.items.ContainsKey(name))
                {
                    this.items[name] = value;
                }
            }
        }
    }
}
