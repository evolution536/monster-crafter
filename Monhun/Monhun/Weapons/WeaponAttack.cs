using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Monhun.Entities;
using Microsoft.Xna.Framework.Graphics;
using Monhun.Monsters;


namespace Monhun.Weapons
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class WeaponAttack : Entity
    {
        GameMain game;
        FacingDirection facing;
        string weaponName;
        int skillDuration;
        int weaponDamage;

        public WeaponAttack(GameMain game, string name, Texture2D texture, Vector2 position, FacingDirection facing, string weaponName, int skillDuration, int weaponDamage)
            : base(name, texture, position)
        {
            this.facing = facing;
            this.weaponName = weaponName;
            this.skillDuration = skillDuration;
            this.weaponDamage = weaponDamage;
            this.game = game;
        }

        public override void Update(GameTime gameTime)
        {
            if (base.CollisionAgainst != null)
            {
                if (base.CollisionAgainst is Potatis)
                {
                    (base.CollisionAgainst as Potatis).HealthPoints -= this.weaponDamage;
                }
                else if (base.CollisionAgainst is SuckABus)
                {
                    (base.CollisionAgainst as SuckABus).HealthPoints -= this.weaponDamage;
                }
                else if (base.CollisionAgainst is InvisibleFatDragon)
                {
                    (base.CollisionAgainst as InvisibleFatDragon).HealthPoints -= this.weaponDamage;
                }
                else if (base.CollisionAgainst is RedWingsGreenDragon)
                {
                    (base.CollisionAgainst as RedWingsGreenDragon).HealthPoints -= this.weaponDamage;
                }
                else if (base.CollisionAgainst is BawbGhais)
                {
                    (base.CollisionAgainst as BawbGhais).HealthPoints -= this.weaponDamage;
                }

                base.CollisionAgainst = null;
                EntityManager.GetInstance(null).RemoveEntity(this.Name);
            }

            if (--this.skillDuration <= 0)
            {
                EntityManager.GetInstance(null).RemoveEntity(this.Name);
            }
        }

        /// <summary>
        /// The weapon attack is moveable because it has to be collision-checked.
        /// </summary>
        public override bool IsMoveable
        {
            get { return true; }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(base.texture, base.position, null, Color.White, 0f, Vector2.Zero, 2, SpriteEffects.None, 0f);

            /*// Draw weapon hits against monsters.
            if (base.CollisionAgainst is Potatis)
            {
                spriteBatch.DrawString(this.game.Font, String.Format("{0} damage against Potatis!", this.weaponDamage)
                    , new Vector2(this.position.X - 30, this.position.Y - 60), Color.DarkGreen);
            }
            else if (base.CollisionAgainst is SuckABus)
            {
                spriteBatch.DrawString(this.game.Font, String.Format("{0} damage against SuckABus!", this.weaponDamage)
                    , new Vector2(this.position.X - 30, this.position.Y - 60), Color.DarkGreen);
            }
            else if (base.CollisionAgainst is InvisibleFatDragon)
            {
                spriteBatch.DrawString(this.game.Font, String.Format("{0} damage against InvisibleFatDragon!", this.weaponDamage)
                    , new Vector2(this.position.X - 30, this.position.Y - 60), Color.DarkGreen);
            }
            else if (base.CollisionAgainst is RedWingsGreenDragon)
            {
                spriteBatch.DrawString(this.game.Font, String.Format("{0} damage against RedWingsGreenDragon!", this.weaponDamage)
                    , new Vector2(this.position.X - 30, this.position.Y - 60), Color.DarkGreen);
            }
            else if (base.CollisionAgainst is BawbGhais)
            {
                spriteBatch.DrawString(this.game.Font, String.Format("{0} damage against BawbGhais!", this.weaponDamage)
                    , new Vector2(this.position.X - 30, this.position.Y - 60), Color.DarkGreen);
            }*/
        }
    }
}
