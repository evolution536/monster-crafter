using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;

namespace Monhun
{
    class SettingsMenu : IDrawable, IUpdateable
    {
        GameMain game;
        private string navLocation;
        Texture2D backgroundTexture;
        int buttonTimeout = 10;

        Texture2D leftButton;
        Rectangle leftButtonRectangle;
        Texture2D rightButton;
        Rectangle rightButtonRectangle;

        public SettingsMenu(GameMain game)
        {
            this.game = game;
            this.backgroundTexture = game.Content.Load<Texture2D>("Images/MonCraftMenuBackground");

            this.leftButton = game.Content.Load<Texture2D>("Images/leftSettings");
            this.rightButton = game.Content.Load<Texture2D>("Images/rightSettings");
            this.leftButtonRectangle = new Rectangle(175, 55, leftButton.Bounds.Width, this.leftButton.Height);
            this.rightButtonRectangle = new Rectangle(280, 55, rightButton.Bounds.Width, this.rightButton.Height);
        }

        public void Update(Microsoft.Xna.Framework.GameTime gametime)
        {
            this.navLocation = "SettingsMenu";

            // Timeout to prevent reclicking the inventory and craft button too fast.
            if (this.buttonTimeout <= 9)
            {
                if (--this.buttonTimeout <= 0)
                {
                    this.buttonTimeout = 10;
                }
            }

            TouchCollection col = TouchPanel.GetState();
            for (int i = 0; i < col.Count; i++)
            {
                TouchLocation tl = col[i];
                if ((tl.State == TouchLocationState.Pressed) || (tl.State == TouchLocationState.Moved) && this.buttonTimeout >= 10)
                {
                    Vector2 newPos = new Vector2(tl.Position.X, tl.Position.Y);

                    if (this.leftButtonRectangle.Contains((int)newPos.X, (int)newPos.Y))
                    {
                        if (Settings.settingVolume > 0f)
                        {
                            Settings.settingVolume -= 10;
                        }
                    }

                    if (this.rightButtonRectangle.Contains((int)newPos.X, (int)newPos.Y))
                    {
                        if (Settings.settingVolume < 100)
                        {
                            Settings.settingVolume += 10;
                        }
                    }
                    --this.buttonTimeout;
                }
            }
        }

        public void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch batch)
        {
            batch.Begin();

            // Background
            batch.Draw(this.backgroundTexture, this.backgroundTexture.Bounds, Color.White);
            batch.DrawString(this.game.Font, "Volume:", new Vector2(50, 50), Color.Red, 0f, new Vector2(0, 0), 1.5f, SpriteEffects.None, 0f);
            batch.DrawString(this.game.Font, (Settings.settingVolume).ToString() + "%", new Vector2(200, 50), Color.Red, 0f, new Vector2(0, 0), 1.5f, SpriteEffects.None, 0f);
            batch.Draw(this.leftButton, this.leftButtonRectangle, Color.White);
            batch.Draw(this.rightButton, this.rightButtonRectangle, Color.White);

            batch.End();
        }

        /// <summary>
        /// Indicates whether the start button has been pressed yet.
        /// </summary>
        public string ShouldCloseMenu
        {
            get
            {
                return this.navLocation;
            }
        }
    }
}
