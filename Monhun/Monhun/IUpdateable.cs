using Microsoft.Xna.Framework;

namespace Monhun
{
    /// <summary>
    /// Defines an interface that describes functions to make classes updateable by each game update tick.
    /// </summary>
    public interface IUpdateable
    {
        /// <summary>
        /// Called when the game update functions is called.
        /// </summary>
        /// <param name="gametime">The gametime object passed from the main update method.</param>
        void Update(GameTime gametime);
    }
}
