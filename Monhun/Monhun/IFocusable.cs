﻿using Microsoft.Xna.Framework;

namespace Monhun
{
    /// <summary>
    /// Represents an object that can be used as focus point for the camera.
    /// </summary>
    public interface IFocusable
    {
        Vector2 Position { get; }
    }
}