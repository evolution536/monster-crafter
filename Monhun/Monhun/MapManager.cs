﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Monhun
{
    /// <summary>
    /// Manages the map including all non-moveable entities.
    /// </summary>
    public sealed class MapManager : IDrawable
    {
        GameMain game;

        /// <summary>
        /// Initializes a new instance of the MapManager.
        /// </summary>
        public MapManager(GameMain game)
        {
            this.game = game;
        }

        /// <summary>
        /// Draws the entire map including non-moveable entities.
        /// </summary>
        /// <param name="spriteBatch">The spritebatch parameter from the main class.</param>
        public void Draw(SpriteBatch spriteBatch)
        {

        }
    }
}