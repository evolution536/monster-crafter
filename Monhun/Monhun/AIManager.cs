﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Monhun.Entities;
using Monhun.Monsters;

namespace Monhun
{
    /// <summary>
    /// Represents the Artificial Intelligence manager, which manages all the bots.
    /// </summary>
    public sealed class AIManager : IUpdateable
    {
        static AIManager instance;

        AIManager(GameMain game)
        {
            this.game = game;
            this.managedbots = new List<IArtificialIntelligence>();
        }

        /// <summary>
        /// Returns the one and only instance of AIManager available.
        /// </summary>
        public static AIManager GetInstance(GameMain game)
        {
            if (instance == null)
            {
                instance = new AIManager(game);
            }

            return instance;
        }

        GameMain game;
        List<IArtificialIntelligence> managedbots;

        public List<IArtificialIntelligence> Managedbots
        {
            get { return managedbots; }
        }

        int nextMobNumber;

        /// <summary>
        /// Adds a new AI bot to the manager. Beware that because of the lack of conditional fields no checks are being done.
        /// </summary>
        /// <param name="ai">The AI bot to add to the manager.</param>
        public void AddAIEntity(IArtificialIntelligence ai)
        {
            this.managedbots.Add(ai);
        }

        /// <summary>
        /// Updates the AIManager.
        /// </summary>
        /// <param name="gameTime">The gametime parameter from the main class.</param>
        public void Update(GameTime gameTime)
        {
            /*if (this.managedbots.Count < 10)
            {
                RandomMonsterSpawn(10 - this.managedbots.Count);
            }*/
        }

        /// <summary>
        /// Clears the managed AI bots. This action cannot be undone.
        /// </summary>
        public void ClearAI()
        {
            EntityManager em = EntityManager.GetInstance(null);
            for (int i = 0; i < this.managedbots.Count; i++)
            {
                em.RemoveEntity((this.managedbots[i] as Entity).Name);
            }

            this.managedbots.Clear();
        }

        /// <summary>
        /// Removes the AI entity with the specified name from the manager. If the name is not found, nothing is done.
        /// </summary>
        /// <param name="ai">The AI entity to remove from the list.</param>
        public void RemoveAIEntity(IArtificialIntelligence ai)
        {
            this.managedbots.ForEach(delegate(IArtificialIntelligence temp)
            {
                Entity tmpEnt = temp as Entity;
                if (tmpEnt != null && tmpEnt.Name == (ai as Entity).Name)
                {
                    this.managedbots.Remove(temp);
                    //EntityManager.GetInstance(null).RemoveEntity(tmpEnt.Name);
                }
            });
        }

        /// <summary>
        /// Private method that can be used to randomly spawn monsters.
        /// <param name="count">The amount of bots to spawn.</param>
        /// </summary>
        void RandomMonsterSpawn(int count)
        {
            Random r = new Random();
            int tempCounter = 0;
            EntityManager em = EntityManager.GetInstance(null);

            while (tempCounter < count)
            {
                Vector2 mobPos = new Vector2((float)r.Next(-2500, 2500), (float)r.Next(-2000, 2000));
                if (!em.PositionCollides(mobPos))
                {
                    unchecked { ++this.nextMobNumber; }

                    // Spawn a new monster.
                    Entity newAi = null;
                    switch (r.Next(0, 2))
                    {
                        case 0:
                            newAi = new Potatis(this.game, String.Format("AIBot{0}", this.nextMobNumber)
                                , this.game.Content.Load<Texture2D>("Images/Monster/Potatis/Left1"), mobPos);
                            break;
                        case 1:
                            newAi = new SuckABus(this.game, String.Format("AIBot{0}", this.nextMobNumber)
                                , this.game.Content.Load<Texture2D>("Images/Monster/SuckABus/Left1"), mobPos);
                            break;
                    }
                    
                    em.AddEntity(newAi);
                    this.managedbots.Add(newAi as IArtificialIntelligence);

                    // Only increment counter if the monster was actually spawned.
                    ++tempCounter;
                }
            }
        }

        public void MonsterSpawn(int count, string monster)
        {
            Random r = new Random();
            int tempCounter = 0;
            EntityManager em = EntityManager.GetInstance(null);

            while (tempCounter < count)
            {
                Vector2 mobPos = new Vector2((float)r.Next(-100, 100), (float)r.Next(-100, 100));
                //Vector2 mobPos = new Vector2((float)r.Next(-2500, 2500), (float)r.Next(-2000, 2000));
                if (!em.PositionCollides(mobPos))
                {
                    unchecked { ++this.nextMobNumber; }

                    // Spawn a new monster.
                    Entity newAi = null;
                    switch (monster)
                    {
                        case "Potatis":
                            newAi = new Potatis(this.game, String.Format("AIBot{0}", this.nextMobNumber)
                                , this.game.Content.Load<Texture2D>("Images/Monster/Potatis/Left1"), mobPos);
                            break;
                        case "SuckABus":
                            newAi = new SuckABus(this.game, String.Format("AIBot{0}", this.nextMobNumber)
                                , this.game.Content.Load<Texture2D>("Images/Monster/SuckABus/Left1"), mobPos);
                            break;
                        case "InvisibleFatDragon":
                            newAi = new InvisibleFatDragon(this.game, String.Format("AIBot{0}", this.nextMobNumber)
                                , this.game.Content.Load<Texture2D>("Images/Monster/InvisibleFatDragon/Left1"), mobPos);
                            break;
                        case "RedWingsGreenDragon":
                            newAi = new RedWingsGreenDragon(this.game, String.Format("AIBot{0}", this.nextMobNumber)
                                , this.game.Content.Load<Texture2D>("Images/Monster/RedWingsGreenDragon/Left1"), mobPos);
                            break;
                        case "BawbGhais":
                            newAi = new BawbGhais(this.game, String.Format("AIBot{0}", this.nextMobNumber)
                                , this.game.Content.Load<Texture2D>("Images/Monster/BawbGhais/Left1"), mobPos);
                            break;
                    }

                    em.AddEntity(newAi);
                    this.managedbots.Add(newAi as IArtificialIntelligence);

                    // Only increment counter if the monster was actually spawned.
                    ++tempCounter;
                }
            }
        }
    }
}