﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Monhun.Entities
{
    /// <summary>
    /// Represents a pool of water somewhere in the map. Since a player cannot swim, collision must be detectable.
    /// </summary>
    public sealed class WaterPool : Entity
    {
        /// <summary>
        /// Initializes a new instance of the WaterPool class using specified parameters.
        /// </summary>
        /// <param name="name">The name of the water pool.</param>
        /// <param name="texture">The texture associated with the Water pool.</param>
        /// <param name="center">The position it should be located.</param>
        public WaterPool(string name, Texture2D texture, Vector2 center) : base(name, texture, center)
        {

        }

        /// <summary>
        /// Indicates that a pool of water is not moveable. Needed for proper collision detection.
        /// </summary>
        public override bool IsMoveable
        {
            get { return false; }
        }

        /// <summary>
        /// Updates the water pool.
        /// </summary>
        /// <param name="gameTime">The gametime parameter from the main class.</param>
        public override void Update(GameTime gameTime)
        {
            // a water pool doesn't move
        }

        /// <summary>
        /// Draws the water pool.
        /// </summary>
        /// <param name="spriteBatch">The spritebatch parameter from the main class.</param>
        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(this.texture, this.position, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
        }
    }
}