﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;
using Monhun.Skills;
using Monhun.Weapons;
using Monhun.Monsters;

namespace Monhun.Entities
{
    /// <summary>
    /// Indicates which direction the player face currently is pointing to.
    /// </summary>
    public enum FacingDirection
    {
        /// <summary>
        /// Indicates that the player is facing left.
        /// </summary>
        Left,

        /// <summary>
        /// Indicates that the player is facing up.
        /// </summary>
        Up,

        /// <summary>
        /// Indicates that the player is facing right.
        /// </summary>
        Right,

        /// <summary>
        /// Indicates that the player is facing down.
        /// </summary>
        Down
    };

    /// <summary>
    /// Indicates whether the main player is currently using normal attacks or skills.
    /// </summary>
    public enum AttackOrSkill
    {
        /// <summary>
        /// Indicates that the main player is currently using normal attacks.
        /// </summary>
        Attack,

        /// <summary>
        /// Indicates that the main player is currently using skills.
        /// </summary>
        Skill,

        /// <summary>
        /// Unlocks other skills or attacks when one is not used anymore
        /// </summary>
        Unlock
    };

    /// <summary>
    /// Represents the main player in the game.
    /// </summary>
    public sealed class Player : Entity, ILivingEntity
    {
        const int SkillShoutDuration = 75;

        GameMain game;
        Vector2 velocity;
        string weaponName;
        Weapon weapon;
        Texture2D weaponTexture;
        Skill[] playerSkills;
        FacingDirection facing;
        AttackOrSkill typeAttack;

        float healthPoints;
        float manaPoints;
        int stepCount = 100;
        int skillShoutTime;
        int playerLevel;
        string skillShout;
        bool mustDrawSkillShout;

        public Player(GameMain game, Texture2D texture, Vector2 center, string name, string weaponName, Vector2 velocity)
            : base(name, texture, center)
        {
            this.game = game;
            if (this.weaponName == null)
            {
                this.weaponName = weaponName;
                this.weapon = Weapon.GetInstance(this.game).getWeapon(this.weaponName);
            }

            this.velocity = velocity;

            this.healthPoints = 100;
            this.manaPoints = 100;
            this.skillShoutTime = SkillShoutDuration;
            this.typeAttack = AttackOrSkill.Unlock;

            playerSkills = new Skill[1];
            playerSkills[0] = new FartEscape(game);
        }

        /// <summary>
        /// Indicates that a player is a moveable object. Needed for proper collision detection.
        /// </summary>
        public override bool IsMoveable
        {
            get { return true; }
        }

        /// <summary>
        /// Updates the player, correcting its position using pressed keys.
        /// </summary>
        public override void Update(GameTime gameTime)
        {
            if (this.healthPoints <= 0 && this.game.NavLocation == "MonsterHunt")
            {
                this.game.ShouldEndGame = true;
            }

            if (this.IsColliding)
            {
                // Collision detected, restore previous position.
                this.virtualPosition = this.position;
            }
            else
            {
                // No collision detected, commit virtual position.
                this.position = virtualPosition;
            }

            // If there is collision with a monster, decrease HP.
            if (this.CollisionAgainst is Potatis)
            {
                this.healthPoints -= 0.25f;
            }
            else if (this.CollisionAgainst is SuckABus)
            {
                this.healthPoints -= 0.50f;
            }
            else if (this.CollisionAgainst is InvisibleFatDragon)
            {
                this.healthPoints -= 1.0f;
            }
            else if (this.CollisionAgainst is RedWingsGreenDragon)
            {
                this.healthPoints -= 1.0f;
            }
            else if (this.CollisionAgainst is BawbGhais)
            {
                this.healthPoints -= 1.25f;
            }

            TouchCollection col = TouchPanel.GetState();
            for (int i = 0; i < col.Count; i++)
            {
                TouchLocation tl = col[i];
                if ((tl.State == TouchLocationState.Pressed) || (tl.State == TouchLocationState.Moved))
                {
                    Vector2 newPos = new Vector2(tl.Position.X, tl.Position.Y);

                    if (game.GameInputController[0].Bounds.Contains((int)newPos.X, (int)newPos.Y))
                    {
                        this.virtualPosition.X -= (float)gameTime.ElapsedGameTime.Milliseconds / 10.0f * 1.0f;
                        if (stepCount < 400)
                        {
                            this.texture = this.game.Content.Load<Texture2D>("Images/Characters/Ack Joff/Left" + stepCount.ToString().Substring(0, 1));
                            stepCount += 20;
                        }
                        else
                        {
                            this.texture = this.game.Content.Load<Texture2D>("Images/Characters/Ack Joff/Left2");
                            stepCount = 100;
                        }
                        this.facing = FacingDirection.Left;
                    }
                    else if (game.GameInputController[1].Bounds.Contains((int)newPos.X, (int)newPos.Y))
                    {
                        this.virtualPosition.X += (float)gameTime.ElapsedGameTime.Milliseconds / 10.0f * 1.0f;
                        if (stepCount < 400)
                        {
                            this.texture = this.game.Content.Load<Texture2D>("Images/Characters/Ack Joff/Right" + stepCount.ToString().Substring(0, 1));
                            stepCount += 20;
                        }
                        else
                        {
                            this.texture = this.game.Content.Load<Texture2D>("Images/Characters/Ack Joff/Right2");
                            stepCount = 100;
                        }
                        this.facing = FacingDirection.Right;
                    }
                    else if (game.GameInputController[2].Bounds.Contains((int)newPos.X, (int)newPos.Y))
                    {
                        this.virtualPosition.Y -= (float)gameTime.ElapsedGameTime.Milliseconds / 10.0f * 1.0f;
                        if (stepCount < 400)
                        {
                            this.texture = this.game.Content.Load<Texture2D>("Images/Characters/Ack Joff/Up" + stepCount.ToString().Substring(0, 1));
                            stepCount += 20;
                        }
                        else
                        {
                            this.texture = this.game.Content.Load<Texture2D>("Images/Characters/Ack Joff/Up2");
                            stepCount = 100;
                        }
                        this.facing = FacingDirection.Up;
                    }
                    else if (game.GameInputController[3].Bounds.Contains((int)newPos.X, (int)newPos.Y))
                    {
                        this.virtualPosition.Y += (float)gameTime.ElapsedGameTime.Milliseconds / 10.0f * 1.0f;
                        if (stepCount < 400)
                        {
                            this.texture = this.game.Content.Load<Texture2D>("Images/Characters/Ack Joff/Down" + stepCount.ToString().Substring(0, 1));
                            stepCount += 20;
                        }
                        else
                        {
                            this.texture = this.game.Content.Load<Texture2D>("Images/Characters/Ack Joff/Down2");
                            stepCount = 100;
                        }
                        this.facing = FacingDirection.Down;
                    }

                    if (game.GameInputController[4].Bounds.Contains((int)newPos.X, (int)newPos.Y))
                    {
                        //Attack btn
                        if (typeAttack == AttackOrSkill.Unlock)
                        {
                            skillShoutTime = 25;
                            skillShout = "Ultimate super normal slash!";
                            this.mustDrawSkillShout = true;
                            typeAttack = AttackOrSkill.Attack;
                        }
                    }
                    else if (game.GameInputController[5].Bounds.Contains((int)newPos.X, (int)newPos.Y))
                    {
                        //Skill btn
                        if (typeAttack == AttackOrSkill.Unlock)
                        {
                            skillShout = "Super weird green cloud!";
                            this.mustDrawSkillShout = true;
                            typeAttack = AttackOrSkill.Skill;
                        }
                    }

                    switch (this.facing)
                    {
                        case FacingDirection.Up:
                            this.weaponTexture = this.game.Content.Load<Texture2D>("Images/Weapons/" + this.weapon.GetType().Name + "/" + this.weapon.Name + "/Up");
                            break;
                        case FacingDirection.Down:
                            this.weaponTexture = this.game.Content.Load<Texture2D>("Images/Weapons/" + this.weapon.GetType().Name + "/" + this.weapon.Name + "/Down");
                            break;
                        case FacingDirection.Left:
                            this.weaponTexture = this.game.Content.Load<Texture2D>("Images/Weapons/" + this.weapon.GetType().Name + "/" + this.weapon.Name + "/Left");
                            break;
                        case FacingDirection.Right:
                            this.weaponTexture = this.game.Content.Load<Texture2D>("Images/Weapons/" + this.weapon.GetType().Name + "/" + this.weapon.Name + "/Right");
                            break;
                    }
                }
            }

            // Count down time in order to know when the skill shout should not be drawn anymore.
            if (skillShoutTime > 0 && mustDrawSkillShout)
            {
                skillShoutTime--;
            }

            if (skillShoutTime > 0 && mustDrawSkillShout && typeAttack == AttackOrSkill.Attack)
            {
                if (!EntityManager.GetInstance(null).CheckEntityExist("WeaponAttack"))
                {
                    Vector2 placedWeapon = this.position;
                    switch (this.facing)
                    {
                        case FacingDirection.Up:
                            placedWeapon.X += 38;
                            placedWeapon.Y -= 28;
                            break;
                        case FacingDirection.Down:
                            placedWeapon.X += 12;
                            placedWeapon.Y += 40;
                            break;
                        case FacingDirection.Left:
                            placedWeapon.X -= 23;
                            break;
                        case FacingDirection.Right:
                            placedWeapon.X += 18 + 33;
                            placedWeapon.Y += 5;
                            break;
                    }
                    ((LongSword)this.weapon).attackEnemy(this.facing, this.weapon.Name, this.game.Content.Load<Texture2D>("Images/Weapons/LongSword/" + this.weapon.Name + "/" + this.facing + "Slash"), placedWeapon);
                }
                skillShoutTime = SkillShoutDuration;
                typeAttack = AttackOrSkill.Unlock;
            }

            // Skill casting block. (mana points are fixed now, but should differ between skills.)
            if (skillShoutTime > 0 && mustDrawSkillShout && typeAttack == AttackOrSkill.Skill && this.manaPoints >= 20f)
            {
                // Only cast the skill if the skill isn't already being cast.
                string name = "Fart Escape";
                if (!EntityManager.GetInstance(null).CheckEntityExist(name))
                {
                    // Decrease player mana points when casting skill.
                    this.manaPoints -= 20;

                    switch (this.facing)
                    {
                        case FacingDirection.Up:
                            this.virtualPosition = this.playerSkills[0].Cast(this.position, this.facing, name, this.game.Content.Load<Texture2D>("Images/Skills/FartDown"));
                            break;
                        case FacingDirection.Down:
                            this.virtualPosition = this.playerSkills[0].Cast(this.position, this.facing, name, this.game.Content.Load<Texture2D>("Images/Skills/FartUp"));
                            break;
                        case FacingDirection.Left:
                            this.virtualPosition = this.playerSkills[0].Cast(this.position, this.facing, name, this.game.Content.Load<Texture2D>("Images/Skills/FartRight"));
                            break;
                        case FacingDirection.Right:
                            this.virtualPosition = this.playerSkills[0].Cast(this.position, this.facing, name, this.game.Content.Load<Texture2D>("Images/Skills/FartLeft"));
                            break;
                    }
                }

                skillShoutTime = SkillShoutDuration;
                typeAttack = AttackOrSkill.Unlock;
            }

            // Make sure the skill shout will disappear in a specified amount of time.
            if (skillShoutTime <= 0)
            {
                this.mustDrawSkillShout = false;
                skillShoutTime = SkillShoutDuration;
                typeAttack = AttackOrSkill.Unlock;
            }

            // Mana regeneration
            if (this.manaPoints < 100f)
            {
                this.manaPoints += 0.25f;
            }
        }

        /// <summary>
        /// Draws the player.
        /// </summary>
        public override void Draw(SpriteBatch batch)
        {
            batch.Draw(this.texture, this.position, null, Color.White, 0f, Vector2.Zero, 2, SpriteEffects.None, 0f);
            if (this.weaponTexture != null)
            {
                Vector2 placedWeapon = this.position;
                switch (this.facing)
                {
                    case FacingDirection.Up:
                        placedWeapon.X += 38;
                        placedWeapon.Y -= 18;
                        batch.Draw(this.weaponTexture, placedWeapon, null, Color.White, 0f, Vector2.Zero, 2, SpriteEffects.None, 0f);
                        break;
                    case FacingDirection.Down:
                        placedWeapon.X += 12;
                        placedWeapon.Y += 40;
                        batch.Draw(this.weaponTexture, placedWeapon, null, Color.White, 0f, Vector2.Zero, 2, SpriteEffects.None, 0f);
                        break;
                    case FacingDirection.Left:
                        placedWeapon.X -= 23;
                        batch.Draw(this.weaponTexture, placedWeapon, null, Color.White, 0f, Vector2.Zero, 2, SpriteEffects.None, 0f);
                        break;
                    case FacingDirection.Right:
                        placedWeapon.X += 18 + 33;
                        batch.Draw(this.weaponTexture, placedWeapon, null, Color.White, 0f, Vector2.Zero, 2, SpriteEffects.None, 0f);
                        break;
                }
            }

            // Draw skill shout while the skill is cast.
            if (this.mustDrawSkillShout)
            {
                batch.DrawString(this.game.Font, skillShout, new Vector2(this.position.X - 30, this.position.Y - 30), Color.Black);
            }

            // Draw monster damage collision while so.
            if (this.CollisionAgainst is Potatis)
            {
                batch.DrawString(this.game.Font, "Potatis bit you!", new Vector2(this.position.X - 30, this.position.Y - 60), Color.DarkRed);
            }
            else if (this.CollisionAgainst is SuckABus)
            {
                batch.DrawString(this.game.Font, "SuckABus pooped on you!", new Vector2(this.position.X - 30, this.position.Y - 60), Color.DarkRed);
            }
            else if (this.CollisionAgainst is RedWingsGreenDragon)
            {
                batch.DrawString(this.game.Font, "The dragon touched you!", new Vector2(this.position.X - 30, this.position.Y - 60), Color.DarkRed);
            }
            else if (this.CollisionAgainst is InvisibleFatDragon)
            {
                batch.DrawString(this.game.Font, "The fat burned you!", new Vector2(this.position.X - 30, this.position.Y - 60), Color.DarkRed);
            }
            else if (this.CollisionAgainst is BawbGhais)
            {
                batch.DrawString(this.game.Font, "Stabbed you!", new Vector2(this.position.X - 30, this.position.Y - 60), Color.DarkRed);
            }
        }

        public void resetPlayer()
        {
            this.Position = new Vector2(0, 0);
            this.virtualPosition = this.position;
            this.HealthPoints = 100;
            this.ManaPoints = 100;
        }

        /// <summary>
        /// Gets or sets the amount of health points the player currently has.
        /// </summary>
        public float HealthPoints
        {
            get
            {
                return this.healthPoints;
            }
            set
            {
                this.healthPoints = value;
            }
        }

        /// <summary>
        /// Gets or sets the amount of mana points the player currently has.
        /// </summary>
        public float ManaPoints
        {
            get
            {
                return this.manaPoints;
            }
            set
            {
                this.manaPoints = value;
            }
        }

        /// <summary>
        /// Gets or sets the level of the player.
        /// </summary>
        public int PlayerLevel
        {
            get
            {
                return this.playerLevel;
            }
            set
            {
                this.playerLevel = value;
            }
        }

        public string WeaponName
        {
            get { return weaponName; }
            set { weaponName = value; }
        }

        public Weapon Weapon
        {
            get { return weapon; }
            set { weapon = value; }
        }
    }
}