﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Monhun.Entities
{
    /// <summary>
    /// Represents a desert ruin that is drawn on the map. This is a non-moveable entity like the pokemon tree.
    /// </summary>
    public sealed class IceWorldEntity : Entity
    {
        /// <summary>
        /// Initializes a new instance of the IceWorldEntity entity using specified parameters.
        /// </summary>
        /// <param name="name">The name of the entity.</param>
        /// <param name="texture">The texture associated with the entity.</param>
        /// <param name="center">the position of the entity.</param>
        public IceWorldEntity(string name, Texture2D texture, Vector2 center) : base(name, texture, center) { }

        /// <summary>
        /// Indicates that a IceWorldEntity is not moveable. Needed for proper collision detection.
        /// </summary>
        public override bool IsMoveable
        {
            get { return false; }
        }

        public override void Update(GameTime gameTime)
        {
            // a IceWorldEntity doesn't move.
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(this.texture, this.position, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
        }
    }
}