using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Monhun.Entities;
using Monhun.Monsters;

namespace Monhun.Entities
{
    /// <summary>
    /// The entity that gets cast when the player uses the fart escape skill.
    /// </summary>
    public class FartEscapeEntity : Entity
    {
        GameMain game;
        private FacingDirection skillDirection;
        private int skillDuration;

        /// <summary>
        /// Initializes a new instance of the FartEscapeEntity class using specified parameters.
        /// </summary>
        /// <param name="game">The game parameter to access the spritefont.</param>
        /// <param name="Facing">The facing direction the fart should go.</param>
        /// <param name="skillDuration">The duration of the skill.</param>
        /// <param name="name">The name the of the skill.</param>
        /// <param name="texture">The texture the entity gets.</param>
        /// <param name="position">The starting position from where the fart will travel.</param>
        public FartEscapeEntity(GameMain game, FacingDirection Facing, int skillDuration, string name, Texture2D texture, Vector2 position)
            : base(name, texture, position)
        {
            this.game = game;
            this.skillDirection = Facing;
            this.skillDuration = skillDuration;

            switch (this.skillDirection)
            {
                case FacingDirection.Up:
                    base.virtualPosition.Y += 38;
                    break;
                case FacingDirection.Down:
                    base.virtualPosition.Y -= 38;
                    break;
                case FacingDirection.Left:
                    base.virtualPosition.X += 38;
                    break;
                case FacingDirection.Right:
                    base.virtualPosition.X -= 38;
                    break;
            }
        }

        /// <summary>
        /// Gets if this entity is declared moveable. In this case it is.
        /// </summary>
        public override bool IsMoveable
        {
            get { return true; }
        }

        /// <summary>
        /// Updates the entity.
        /// </summary>
        /// <param name="gameTime">The main gametime parameter.</param>
        public override void Update(GameTime gameTime)
        {
            if (base.CollisionAgainst != null)
            {
                //todo Damage output
                Entity collisionAgainst = base.CollisionAgainst;
                if (collisionAgainst.GetType() == typeof(Potatis))
                {
                    (collisionAgainst as Potatis).HealthPoints -= 50;
                }
                else if (collisionAgainst.GetType() == typeof(SuckABus))
                {
                    (collisionAgainst as SuckABus).HealthPoints -= 50;
                }
                else if (collisionAgainst.GetType() == typeof(InvisibleFatDragon))
                {
                    (collisionAgainst as InvisibleFatDragon).HealthPoints -= 40;
                }
                else if (collisionAgainst.GetType() == typeof(RedWingsGreenDragon))
                {
                    (collisionAgainst as RedWingsGreenDragon).HealthPoints -= 30;
                }
                else if (collisionAgainst.GetType() == typeof(BawbGhais))
                {
                    (collisionAgainst as BawbGhais).HealthPoints -= 25;
                }

                base.CollisionAgainst = null;
                EntityManager.GetInstance(null).RemoveEntity(this.Name);
            }

            if (--this.skillDuration <= 0)
            {
                EntityManager.GetInstance(null).RemoveEntity(this.Name);
            }
        }

        /// <summary>
        /// Draws the entity.
        /// </summary>
        /// <param name="spriteBatch">The main spritebatch parameter.</param>
        public override void Draw(SpriteBatch spriteBatch)
        {
            switch (this.skillDirection)
            {
                case FacingDirection.Up:
                    base.virtualPosition.Y += 2;
                    break;
                case FacingDirection.Down:
                    base.virtualPosition.Y -= 2;
                    break;
                case FacingDirection.Left:
                    base.virtualPosition.X += 2;
                    break;
                case FacingDirection.Right:
                    base.virtualPosition.X -= 2;
                    break;
            }

            spriteBatch.Draw(base.texture, base.virtualPosition, null, Color.White, 0f, Vector2.Zero, 2, SpriteEffects.None, 0f);

            /*// Draw skill hits against monsters.
            if (base.CollisionAgainst is Potatis)
            {
                spriteBatch.DrawString(this.game.Font, "50 damage against Potatis!", new Vector2(this.position.X - 30, this.position.Y - 60), Color.DarkGreen);
            }
            else if (base.CollisionAgainst is SuckABus)
            {
                spriteBatch.DrawString(this.game.Font, "50 damage against SuckABus!", new Vector2(this.position.X - 30, this.position.Y - 60), Color.DarkGreen);
            }
            else if (base.CollisionAgainst is InvisibleFatDragon)
            {
                spriteBatch.DrawString(this.game.Font, "40 damage against InvisibleFatDragon!", new Vector2(this.position.X - 30, this.position.Y - 60), Color.DarkGreen);
            }
            else if (base.CollisionAgainst is RedWingsGreenDragon)
            {
                spriteBatch.DrawString(this.game.Font, "30 damage against RedWingsGreenDragon!", new Vector2(this.position.X - 30, this.position.Y - 60), Color.DarkGreen);
            }
            else if (base.CollisionAgainst is BawbGhais)
            {
                spriteBatch.DrawString(this.game.Font, "25 damage against BawbGhais!", new Vector2(this.position.X - 30, this.position.Y - 60), Color.DarkGreen);
            }*/
        }
    }
}
