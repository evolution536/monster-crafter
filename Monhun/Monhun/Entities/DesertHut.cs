﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Monhun.Entities
{
    /// <summary>
    /// Represents a desert hut that is drawn on the map. This is a non-moveable entity like the pokemon tree.
    /// </summary>
    public sealed class DesertHut : Entity
    {
        /// <summary>
        /// Initializes a new instance of the Desert Hut entity using specified parameters.
        /// </summary>
        /// <param name="name">The name of the entity.</param>
        /// <param name="texture">The texture associated with the entity.</param>
        /// <param name="center">the position of the entity.</param>
        public DesertHut(string name, Texture2D texture, Vector2 center) : base(name, texture, center) { }

        /// <summary>
        /// Indicates that a desert hut is not moveable. Needed for proper collision detection.
        /// </summary>
        public override bool IsMoveable
        {
            get { return false; }
        }

        public override void Update(GameTime gameTime)
        {
            // a desert hut doesn't move.
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(this.texture, this.position, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
        }
    }
}