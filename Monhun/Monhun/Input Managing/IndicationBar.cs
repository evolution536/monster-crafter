﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Monhun.Entities;

namespace Monhun
{
    /// <summary>
    /// Indicates which type of points the indication bar should display.
    /// </summary>
    public enum IndicationType
    {
        /// <summary>
        /// Indicates that the indication bar should display the player's health points.
        /// </summary>
        HealthPoints,
        
        /// <summary>
        /// Indicates that the indication bar should display the player's mana points.
        /// </summary>
        ManaPoints,

        /// <summary>
        /// Indicates that the indication bar should display the player's experience points.
        /// </summary>
        Experience
    }

    /// <summary>
    /// Represents an indication bar to view HP, Mana and EXP values, for example.
    /// </summary>
    public sealed class IndicationBar : IDrawable, IUpdateable
    {
        float max;
        Color color;
        Texture2D barTexture;
        Texture2D texHp;
        Player player;
        float healthPercentage;
        Vector2 upperLeftcorner;
        IndicationType type;

        /// <summary>
        /// Initializes a new instance of the IndicationBar class using specified parameters.
        /// </summary>
        /// <param name="player">The player this indication bar is associated to.</param>
        /// <param name="type">The type of indication bar.</param>
        /// <param name="barTexture">The texture the indication bar will use to draw itself.</param>
        /// <param name="upperLeftCorner">The upper left corner position where the indication bar will be drawn.</param>
        /// <param name="maximum">The maximum value this indication bar is able to display.</param>
        /// <param name="color">The color to display the value in.</param>
        public IndicationBar(Player player, IndicationType type, Texture2D barTexture, Vector2 upperLeftCorner, float maximum, Color color)
        {
            this.player = player;
            this.type = type;
            this.barTexture = barTexture;
            this.upperLeftcorner = upperLeftCorner;
            this.max = maximum;
            this.color = color;
        }

        /// <summary>
        /// Gets or sets the value the bar will display.
        /// </summary>
        public float Value
        {
            get
            {
                switch (this.type)
                {
                    case IndicationType.HealthPoints:
                        return this.player.HealthPoints;
                    case IndicationType.ManaPoints:
                        return this.player.ManaPoints;
                    case IndicationType.Experience:
                        // NOT YET IMPLEMENTED
                        return 0;
                    default:
                        return 0;
                }
            }
            set
            {
                switch (this.type)
                {
                    case IndicationType.HealthPoints:
                        this.player.HealthPoints = value;
                        break;
                    case IndicationType.ManaPoints:
                        this.player.ManaPoints = value;
                        break;
                    case IndicationType.Experience:
                        // NOT YET IMPLEMENTED
                        break;
                }
            }
        }

        /// <summary>
        /// Gets or sets the maximum value the bar can handle.
        /// </summary>
        public float Maximum
        {
            get
            {
                return this.max;
            }
            set
            {
                this.max = value;
            }
        }

        /// <summary>
        /// Gets or sets the color the bar will display.
        /// </summary>
        public Color Color
        {
            get
            {
                return this.color;
            }
            set
            {
                this.color = value;
            }
        }

        /// <summary>
        /// Draws the bar using the specified value and color.
        /// </summary>
        public void Draw(SpriteBatch spriteBatch)
        {
            // Calculate the HP that will be visible in the bar.
            float visibleWidth = (float)this.barTexture.Width * this.healthPercentage;

            // Draw full bar.
            spriteBatch.Draw(this.barTexture, new Rectangle((int)this.upperLeftcorner.X, (int)this.upperLeftcorner.Y
                , this.barTexture.Width, this.barTexture.Height), this.color);

            // Draw the part that indicates the HP left.
            if (this.texHp == null)
            {
                this.texHp = new Texture2D(spriteBatch.GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
                this.texHp.SetData<Color>(new Color[] { this.color });
            }
            
            spriteBatch.Draw(texHp, new Rectangle((int)this.upperLeftcorner.X, (int)this.upperLeftcorner.Y, (int)visibleWidth
                , this.barTexture.Height), this.color);   
        }

        /// <summary>
        /// Updates the bar.
        /// </summary>
        public void Update(GameTime gameTime)
        {
            switch (this.type)
            {
                case IndicationType.HealthPoints:
                    this.healthPercentage = this.player.HealthPoints / this.max;
                    break;
                case IndicationType.ManaPoints:
                    this.healthPercentage = this.player.ManaPoints / this.max;
                    break;
                case IndicationType.Experience:
                    // NOT YET IMPLEMENTED
                    break;
            }      
        }
    }
}