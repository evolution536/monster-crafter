﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Monhun
{
    /// <summary>
    /// Represents a button that can be drawn on-screen and used to handle user input.
    /// </summary>
    public class InputButton : IDrawable
    {
        Texture2D button;
        Rectangle position;

        /// <summary>
        /// Initializes a new instance of the InputButton class using specified parameters.
        /// </summary>
        /// <param name="button">The texture to use as button. Preferrably a transparent image.</param>
        /// <param name="position">The position as Rectangle to draw the button at.</param>
        public InputButton(Texture2D button, Rectangle position)
        {
            this.button = button;
            this.position = position;
        }

        /// <summary>
        /// Draws the button on the screen.
        /// </summary>
        /// <param name="sp">The spritebatch passed from the main draw method.</param>
        public void Draw(SpriteBatch sp)
        {
            sp.Draw(button, position, Color.White);
        }

        /// <summary>
        /// Gets the bounds of the button as a Rectangle.
        /// </summary>
        public Rectangle Bounds
        {
            get
            {
                return this.position;
            }
        }
    }
}