﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Monhun.Entities;
using Monhun.Weapons;

namespace Monhun
{
    /// <summary>
    /// Represents a container that holds all game entities and manages the drawing and updating of them.
    /// </summary>
    public sealed class EntityManager : IUpdateable, IDrawable
    {
        static EntityManager instance;
        List<Entity> mEntities;
        Camera2D camera;

        /// <summary>
        /// Gets the instance of the singleton EntityManager class.
        /// </summary>
        public static EntityManager GetInstance(Camera2D cam)
        {
            if (instance == null)
            {
                instance = new EntityManager();
                instance.camera = cam;
            }

            return instance;
        }

        EntityManager() { this.mEntities = new List<Entity>(); }

        /// <summary>
        /// Adds an entity to the entity manager.
        /// </summary>
        /// <param name="player"></param>
        public void AddEntity(Entity player)
        {
            if ((from temp in this.mEntities where temp.Name == player.Name select temp).Count() > 0)
            {
                throw new EntityExistsException();
            }

            this.mEntities.Add(player);
        }

        /// <summary>
        /// Removes the first entity that matched the given name. If the name is null or empty, the functions returns without action.
        /// </summary>
        /// <param name="name">The name given to remove the entity.</param>
        public void RemoveEntity(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                if((from found in this.mEntities where found.Name == name select found).Count() > 0)
                {
                    Entity temp = (from found in this.mEntities where found.Name == name select found).First();
                    if (temp != null)
                    {
                        this.mEntities.Remove(temp);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the current count of entities managed.
        /// </summary>
        public int EntityCount { get { return this.mEntities.Count; } }

        /// <summary>
        /// Check if the entity exists. If the name is null or empty, the functions returns false.
        /// </summary>
        /// <param name="name">The name given to remove the entity.</param>
        public bool CheckEntityExist(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                try
                {
                    Entity temp = (from found in this.mEntities where found.Name == name select found).First();
                    if (temp != null)
                    {
                        return true;
                    }
                }
                catch
                {
                    return false;
                }
            }
            return false;
        }

        /// <summary>
        /// Clears all entities, setting the entitymanager effectively to inactive state.
        /// </summary>
        public void ClearEntities()
        {
            this.mEntities.Clear();
        }
        
        /// <summary>
        /// Gets the entity that matches the given name.
        /// </summary>
        /// <param name="name">The name to search the entity manager with.</param>
        /// <returns>If the name was found, the entity in case. Otherwise null.</returns>
        public Entity this[string name]
        {
            get
            {
                IEnumerable<Entity> tempRet = from temp in this.mEntities where temp.Name == name select temp;
                return tempRet.Count() > 0 ? tempRet.First() : null;
            }
        }

        /// <summary>
        /// Updates all entities managed.
        /// </summary>
        /// <param name="gameTime">The gametime object from the main game object.</param>
        public void Update(GameTime gameTime)
        {
            for (int i = 0; i < this.mEntities.Count; i++)
            {
                this.mEntities[i].Update(gameTime);
            }

            this.CheckEntityCollisions();
        }

        /// <summary>
        /// Draws all entities managed.
        /// </summary>
        /// <param name="spriteBatch">The spritebatch object from the main game object.</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < this.mEntities.Count; i++)
            {
                if (this.camera.IsInView(this.mEntities[i].EntityRect))
                {
                    this.mEntities[i].Draw(spriteBatch);
                }
            }
        }

        /// <summary>
        /// Checks for collisions between entities and generates a list of collisions so they can be handled dynamically.
        /// </summary>
        public void CheckEntityCollisions()
        {
            // Loop all entities in the entity manager.
            for (int i = 0; i < this.mEntities.Count; i++)
            {
                // Only check collision for the current iterated entity if it is moveable and in the camera view.
                // IArtificialIntelligence objects will always be collision-checked since otherwise they will outwalk the map borders.
                if (this.mEntities[i].IsMoveable && (this.mEntities[i] is IArtificialIntelligence || 
                    this.camera.IsInView(this.mEntities[i].EntityRect)))
                {
                    // Loop through all entities again to match entities to each other.
                    for (int j = 0; j < this.mEntities.Count; j++)
                    {
                        // Check whether the entity does not check collision with itself.
                        if (this.mEntities[i].Name != this.mEntities[j].Name)
                        {
                            // If the weapon attack collides with the player, continue. This must not happen.
                            if (this.mEntities[i] is WeaponAttack && this.mEntities[j] is Player)
                            {
                                continue;
                            }

                            //Extend the rectangle to prevent texture overlapping.
                            Rectangle targetRect = this.mEntities[j].EntityRect;
                            targetRect.X -= 25;
                            targetRect.Y -= 25;
                            targetRect.Width += 25;
                            targetRect.Height += 25;

                            // If the current entity is already colliding with something, do not check collision again because it will
                            // be set to false for certain.
                            if (!this.mEntities[i].IsColliding)
                            {
                                // Check whether the entity collides with another entity's expanded Rect.
                                this.mEntities[i].IsColliding = (this.mEntities[i].EntityRect.Intersects(targetRect)
                                    || targetRect.Contains(this.mEntities[i].EntityRect));

                                //Nog een check of die idioot niet tegen zn eigen wapen aan knalt :P
                                this.mEntities[i].CollisionAgainst = this.mEntities[i].IsColliding ? this.mEntities[j] : null;
                            }

                            // Make sure it does not execute the last line of code below.
                            continue;
                        }

                        // If the player is the only entity in the world, collision will never occur.
                        if (this.mEntities[i] is Player)
                        {
                            this.mEntities[i].IsColliding = false;
                        }
                    }
                }
            } 
        }

        /// <summary>
        /// Checks whether a specified position collides with any of the currently visible entities.
        /// Use this function as AI spawn check in order to not spawn an Entity on a colliding position.
        /// </summary>
        /// <param name="mobPos">The position to check collision to.</param>
        /// <returns>Whether the position is in collision with anything managed.</returns>
        public bool PositionCollides(Vector2 mobPos)
        {
            for (int i = 0; i < this.mEntities.Count; i++)
            {
                if ((this.mEntities[i].EntityRect.Intersects(new Rectangle((int)mobPos.X, (int)mobPos.Y, 100, 100))
                    || this.mEntities[i].EntityRect.Contains((int)mobPos.X, (int)mobPos.Y))
                    && ((mobPos.X > -3000 && mobPos.X < 3000) && (mobPos.Y > -2000 && mobPos.Y < 2000)))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Retrieves the player from the list of entities.
        /// </summary>
        /// <returns>The player in game.</returns>
        public Player GetPlayer()
        {
            return (from temp in this.mEntities where (temp as Player) != null select temp).First() as Player;
        }
    }
}