﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;

namespace Monhun
{
    /// <summary>
    /// Represents the main menu that will be displayed on game start.
    /// </summary>
    public sealed class GameMenu : IDrawable, IUpdateable
    {
        SpriteFont mainMenuSpritefont;
        Texture2D background;
        Texture2D monsterButton;
        Texture2D craftButton;
        Texture2D equipmentButton;
        Texture2D settingsButton;
        Rectangle monsterButtonRectangle;
        Rectangle craftButtonRectangle;
        Rectangle equipmentButtonRectangle;
        Rectangle settingsButtonRectangle;
        GameMain game;
        bool shouldClearAI = true;

        string navLocation = "";

        /// <summary>
        /// Initializes a new instance of the MainMenu class using specified parameters.
        /// </summary>
        /// <param name="game">The GameMain class needed to switch between menu and gameplay</param>
        public GameMenu(GameMain game)
        {
            this.game = game;
            this.mainMenuSpritefont = game.Content.Load<SpriteFont>("MainMenu");
            this.background = game.Content.Load<Texture2D>("Images/MonCraftMenuBackground");
            this.monsterButton = game.Content.Load<Texture2D>("Images/Monster");
            this.craftButton = game.Content.Load<Texture2D>("Images/Crafting");
            this.equipmentButton = game.Content.Load<Texture2D>("Images/Equipment");
            this.settingsButton = game.Content.Load<Texture2D>("Images/Settings");
            this.monsterButtonRectangle = new Rectangle((game.Window.ClientBounds.Width / 2) + 50, 125, monsterButton.Bounds.Width, this.monsterButton.Height);
            this.craftButtonRectangle = new Rectangle((game.Window.ClientBounds.Width / 2) + 50, 125 + 10 + this.craftButton.Height, craftButton.Bounds.Width, this.craftButton.Height);
            this.equipmentButtonRectangle = new Rectangle((game.Window.ClientBounds.Width / 2) + 50, 125 + 20 + (this.equipmentButton.Height * 2), equipmentButton.Bounds.Width, this.equipmentButton.Height);
            this.settingsButtonRectangle = new Rectangle((game.Window.ClientBounds.Width / 2) + 50, this.equipmentButtonRectangle.Location.Y + 20 + this.equipmentButton.Height, settingsButton.Bounds.Width, this.settingsButton.Height);

        }

        /// <summary>
        /// Update sequence for the MainMenu class.
        /// </summary>
        /// <param name="gametime">The gametime parameter passed from the main update method.</param>
        public void Update(GameTime gametime)
        {
            navLocation = "GameMenu";

            if (shouldClearAI)
            {
                AIManager.GetInstance(this.game).ClearAI();
                shouldClearAI = false;
            }

            TouchCollection col = TouchPanel.GetState();
            for (int i = 0; i < col.Count; i++)
            {
                TouchLocation tl = col[i];
                if (tl.State == TouchLocationState.Pressed)
                {
                    Vector2 newPos = new Vector2(tl.Position.X, tl.Position.Y);

                    if (this.monsterButtonRectangle.Contains((int)newPos.X, (int)newPos.Y))
                    {
                        shouldClearAI = true;
                        this.game.MonsterMenu.resetTimeLimit();
                        this.navLocation = "MonsterMenu";
                        break;
                    }
                    if (this.craftButtonRectangle.Contains((int)newPos.X, (int)newPos.Y))
                    {
                        this.navLocation = "Crafting";
                        break;
                    }
                    if (this.equipmentButtonRectangle.Contains((int)newPos.X, (int)newPos.Y))
                    {
                        this.navLocation = "Equipment";
                        break;
                    }
                    if (this.settingsButtonRectangle.Contains((int)newPos.X, (int)newPos.Y))
                    {
                        this.navLocation = "SettingsMenu";
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Draws the main menu.
        /// </summary>
        /// <param name="batch">The spritebatch parameter passed from the main draw method.</param>
        public void Draw(SpriteBatch batch)
        {
            batch.Begin();
            batch.Draw(this.background, background.Bounds, Color.White);
            batch.DrawString(mainMenuSpritefont, "Monster Crafter", new Vector2(200f, 30f), Color.Red);
            batch.Draw(this.monsterButton, this.monsterButtonRectangle, Color.White);
            batch.Draw(this.craftButton, this.craftButtonRectangle, Color.White);
            batch.Draw(this.equipmentButton, this.equipmentButtonRectangle, Color.White);
            batch.Draw(this.settingsButton, this.settingsButtonRectangle, Color.White);
            batch.End();
        }

        /// <summary>
        /// Indicates whether the start button has been pressed yet.
        /// </summary>
        public string ShouldCloseMenu
        {
            get
            {
                return this.navLocation;
            }
        }
    }
}